const HtmlWebpackPlugin = require('html-webpack-plugin');
const InlineChunkHtmlPlugin = require('react-dev-utils/InlineChunkHtmlPlugin');
const path = require('path');


module.exports = {
  mode: 'development',
  entry: './lib/index.js',
  module: {
    rules: [{
      test: /\.scss$/i,
      use: [
        'style-loader',
        'css-loader',
        'sass-loader'
      ]
    }]
  },
  output: {
    path: path.resolve(__dirname, './tmp'),
    filename: '[name].[hash].js'
  },
  plugins: [
    new HtmlWebpackPlugin(),
    new InlineChunkHtmlPlugin(HtmlWebpackPlugin, [/main/])
  ]
};
