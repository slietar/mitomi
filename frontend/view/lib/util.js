export function formatTime(date) {
  if (!date) {
    return '–';
  }

  if (typeof date === 'number') {
    date = new Date(date);
  }

  return `${date.getHours().toString().padStart(2, '0')}:${date.getMinutes().toString().padStart(2, '0')}`;
}

// TODO: Prevent stack overflow when input is invalid.
export function formatDuration(input /* in seconds */) {
  if (typeof input !== 'number') return '0';

  if (input === 0) {
    return 'Instant';
  } if (input < 1) {
    return '< 1 sec';
  } if (input < 60) {
    return `${Math.floor(input)} sec`;
  } if (input < 3600) {
    let min = Math.floor(input / 60);
    let sec = Math.floor(input % 60);
    return `${min} min` + (sec > 0 ? ` ${sec} sec` : '');
  }

  return `${Math.floor(input / 3600)} hours ${formatDuration(Math.floor(input % 3600))}`;
  // return `${input / 3600} hours`;
}

export function formatRemainingTime(time /* seconds */) {
  if (time < 60) {
    return 'Less than one minute left';
  } if (time < 15 * 60) {
    let min = Math.round(time / 60);
    return `${min} minute${min > 1 ? 's' : ''} left`;
  } else {
    let hours = Math.floor(time / 3600);
    let min = Math.round(time % 3600 / 60 / 5) * 5;

    return (hours > 0 ? `${hours} hour${hours > 1 ? 's' : ''} ` : '') + (min > 0 ? `${min} minutes ` : '') + 'left';
  }
}


export function formatCode(value) {
  return value.replace(/{(.*?)}/g, '<code>$1</code>');
}

export function splitArray(arr, fn) {
  let out1 = [];
  let out2 = [];

  arr.forEach((item, index) => {
    (fn(item, index, arr) ? out1 : out2).push(item);
  });

  return [out1, out2];
}
