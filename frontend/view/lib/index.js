import { Component, render } from 'preact';
import 'web-animations-js';

import html from './html.js';

import MockBackend from './backends/mock.js';
import PythonBackend from './backends/python.js';
import WebserialBackend from './backends/webserial.js';

import DeviceSelectionDialog from './components/selection-dialog.js';
import Header from './components/header.js';
import ViewManualControl from './views/manual-control.js';
import ViewProtocolRun from './views/protocol-run.js';

import '../styles/main.scss';


let backend = (() => {
  if (!PythonBackend.isSupported()) {
    return new MockBackend(); // debug
  }

  let backends = [PythonBackend, WebserialBackend, MockBackend];

  for (let Backend of backends) {
    if (Backend.isSupported()) return new Backend();
  }
})();

self.backend = backend;


async function main() {
  await backend.initialize();

  if (backend instanceof PythonBackend) {
    document.addEventListener('keydown', (event) => {
      if (event.key === 'r' && event.metaKey) {
        event.preventDefault();
        backend.reloadView();
      }
    });
  }


  render(html`<${App} />`, document.body);
}



const Pages = [
  { name: 'Manual control', component: ViewManualControl },
  { name: 'Protocol run', component: ViewProtocolRun }
];

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      connection: null,
      dialog: null,
      pageIndex: 0
      // pageIndex: 1 // debug
    };
  }

  componentWillMount() {

  }

  componentDidMount() {
    // this.openSelectionDialog(); // debug
  }

  openSelectionDialog() {
    this.setState({
      dialog: html`<${DeviceSelectionDialog} onSelect=${async (value) => {
        if (value) {
          let connection = null;

          try {
            connection = await backend.connect(value.chip, value.device);
          } catch (err) {
            console.error(err);

            // alert() is blocking otherwise.
            setTimeout(() => {
              alert(err.message);
            }, 100);
          }

          if (connection) {
            this.setState({
              connection,
              dialog: null
            });

            connection.on('close', () => {
              this.setState({
                connection: null
              });
            });

            connection.on('lost', () => {
              alert(`Connection to ${this.state.connection.device.name} has been lost.`);
            });
          }
        } else {
          this.setState({
            dialog: null
          });
        }
      }} />`
    });
  }

  render() {
    return html`
      <nav>
        ${Pages.map((page, index) => {
          return html`<button type="button" class=${this.state.pageIndex === index ? '_active' : ''} onClick=${() => {
            this.setState({ pageIndex: index });
          }}>${page.name}</button>`;
        })}
      </nav>

      <${Header} app=${this} connection=${this.state.connection} title=${Pages[this.state.pageIndex].name} />

      ${Pages.map((page, index) => html`
        <main style=${{display: index === this.state.pageIndex ? 'unset' : 'none'}}>
          <${page.component} app=${this} connection=${this.state.connection} />
        </main>
      `)}

      ${this.state.dialog}
    `;
  }
}


main().catch((err) => {
  console.error(err);
});
