import html from '../html.js';


export default function ValveList(props) {
  let renderedValves = props.chip.valves
    .filter((_valve, valveIndex) => props.valves.includes(valveIndex))
    .sort((a, b) => {
      let ag = a.group != null ? a.group : Infinity;
      let bg = b.group != null ? b.group : Infinity;
      return ag - bg;
    })
    .map((valve) => html`<div class=${'valve-item valve-item--' + (valve.group != null ? valve.group : 'none')}>${valve.name}</div>`)

  if (renderedValves.length < 1) {
    return html`<div class="valve-empty">No valve set</div>`;
  }

  return html`
    <div class="valve-list">
      ${renderedValves}
    </div>
  `;
}
