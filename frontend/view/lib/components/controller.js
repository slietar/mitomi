import { Component, createRef } from 'preact';

import html from '../html.js';


export class Controller extends Component {
  constructor(props) {
    super(props);

    this.state = {
      state: 0,
      trans: 0
    };


    this.unmountController = new AbortController();

    document.body.addEventListener('keyup', (event) => {
      let command = event.key === 'Control' || event.key === 'Meta';

      if (!command) {
        return;
      }

      let { state, trans } = this.state;

      let newState = this.chip.valves.reduce((current, _valve, valveIndex) => {
        let mask = 1 << valveIndex;
        let active = (state & mask) !== 0;
        let selected = (trans & mask) !== 0;

        return current | (active !== selected ? mask : 0);
      }, 0);

      this.setValveState(newState);

      this.setState({
        state: newState,
        trans: 0
      });
    }, { signal: this.unmountController.signal });
  }

  get chip() {
    return this.props.connection.chip;
  }

  componentDidMount() {
    this.props.connection.readAll().then((state) => {
      this.setState({
        state
      });
    });
  }

  componentWillUnmount() {
    this.unmountController.abort();
  }

  setValveState(state) {
    this.props.connection.writeAll(state)
      .then(() => this.props.connection.readAll())
      .then((state) => {
        this.setState({ state });
      });
  }

  render() {
    return html`
      <div>
        ${this.chip.schematic && html`<${SchematicController} chip=${this.chip} state=${this.state.state} trans=${this.state.trans} />`}
        <${ValveController} state=${this.state.state} trans=${this.state.trans} valves=${this.chip.valves} onValveChange=${(event) => {
          let mask = 1 << event.valveIndex;

          let state = (this.state.state & ~mask) | (event.active ? mask : 0);
          let trans = (this.state.trans & ~mask) | (event.selected ? mask : 0);

          if (!event.selected) {
            this.setValveState(state);
          }

          this.setState({
            state,
            trans
          });
        }} />
      </div>
    `;
  }
}


class SchematicController extends Component {
  constructor(props) {
    super(props);

    this.figureRef = createRef();
    this.groups = null;
  }

  componentDidMount() {
    this.figureRef.current.innerHTML = this.props.chip.schematic;

    this.groups = [];

    for (let valveIndex = 0; valveIndex < this.props.chip.valves.length; valveIndex++) {
      let valve = this.props.chip.valves[valveIndex];
      let element = null;

      if (valve.schematic) {
        let [layerIndex, groupIndex] = valve.schematic;
        element = this.figureRef.current.querySelector(`g[data-layer-index="${layerIndex}"][data-group-index="${groupIndex}"]`);
      }

      this.groups.push(element);
      this.updateValve(valveIndex);
    }

    /* Array.from(this.figureRef.current.querySelectorAll('g')).forEach((x, i) => {
      x.addEventListener('click', () => {
        console.log(i, x.attributes);
      });
    }); */
  }

  componentDidUpdate(prevProps) {
    for (let valveIndex = 0; valveIndex < this.props.chip.valves.length; valveIndex++) {
      let mask = 1 << valveIndex;

      if ((mask & this.props.state) !== (mask & prevProps.state) || (mask & this.props.trans) !== (mask & prevProps.trans)) {
        this.updateValve(valveIndex);
      }
    }
  }

  updateValve(valveIndex) {
    if (!this.groups) {
      return;
    }

    let mask = 1 << valveIndex;
    let group = this.groups[valveIndex];

    if (group) {
      group.classList.toggle('_active', (mask & this.props.state) !== 0);
      group.classList.toggle('_selected', (mask & this.props.trans) !== 0);
    }
  }

  render() {
    return html`
      <figure ref=${this.figureRef} />
    `;
  }
}


class ValveController extends Component {
  constructor(props) {
    super(props);
  }

  setValve(valveIndex, active, selected) {
    this.props.onValveChange({
      active,
      selected,
      valveIndex
    });
  }

  render() {
    let { state, trans } = this.props;

    return html`
      <!-- <div class="displaymode-list">
        <div class="displaymode-label">Display:</div>
        <button type="button" class="displaymode-mode _active">relay</button>
        <button type="button" class="displaymode-mode">valve</button>
        <button type="button" class="displaymode-mode">control</button>
        <button type="button" class="displaymode-mode">flow</button>
      </div> -->

      <ul class="valvecontrol-list">
        ${this.props.valves.map((valve, valveIndex) => {
          let mask = 1 << valveIndex;
          let active = (state & mask) !== 0;
          let selected = (trans & mask) !== 0;
          let inputId = 'valve-' + valveIndex;

          return html`
            <li>
              <label for=${inputId} class=${'valvecontrol-item'
                + (active ? ' _active' : '')
                + (selected ? ' _selected' : '')}
                onClick=${(event) => {
                  event.preventDefault();

                  let command = event.metaKey || event.ctrlKey;
                  this.setValve(valveIndex, command ? active : !active, command && !selected);
                }}>
                <div class="valvecontrol-name">${valve.name}</div>
                <div class="valvecontrol-state">${active ? 'P' : 'Unp'}ressurized</div>
                <input type="checkbox" id=${inputId} class="input-checkbox-2" checked=${active} />
              </label>
            </li>
          `;
        })}
      </ul>
    `;
  }
}
