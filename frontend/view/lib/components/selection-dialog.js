import { Component } from 'preact';
import MockBackend from '../backends/mock.js';

import html from '../html.js';
import * as util from '../util.js';


export default class DeviceSelectionDialog extends Component {
  constructor(props) {
    super(props);

    this.state = {
      chip: null,
      chips: null,
      devices: null,
      loading: null,
      showAll: false
    };
  }

  componentDidMount() {
    this.loadList();
  }

  loadList(options = {}) {
    if (this.chips) {
      this.setState({
        chip: null,
        chips: null,
        devices: null
      });
    }

    Promise.all([
      backend.listChips({ reload: options.reload }),
      backend.listDevices({ reload: options.reload })
    ]).then(([chips, devices]) => {
      let sortedChips = chips.sort((a, b) =>
        ((b.name === 'Default') - (a.name === 'Default'))
          || a.name.localeCompare(b.name)
      );

      let selectedChip = this.state.chip && chips.find((chip) => chip.id === this.state.chip.id);

      this.setState({
        chip: selectedChip || sortedChips[0] || null,
        chips: sortedChips,
        devices
      });

      if (backend instanceof MockBackend) {
        queueMicrotask(() => {
          let device = this.state.devices.find((d) => d.supported)

          if (device) {
            this.props.onSelect({ chip: this.state.chips[0], device });
          }
        });
      }
    });
  }

  render() {
    let items = this.state.devices && this.state.devices.filter((device) => this.state.showAll || device.supported);
    let chip = this.state.chip;

    let canPrompt = 'promptDevice' in backend;
    let canRevealChipsDir = 'revealChipsDirectory' in backend;

    return html`
      <dialog class="dialog" open>
        <div class="dialog-container">
          <div class="dialog-contents">
            <h2>Select a device</h2>

            <div class="dialog-settings">
              <select class="input" value=${this.state.chip && this.state.chip.id} onChange=${(event) => {
                this.setState({
                  chip: this.state.chips.find((chip) => chip.id === event.target.value)
                });
              }}>
                ${this.state.chips && (() => {
                  let [builtinChips, userChips] = util.splitArray(this.state.chips, (chip) => chip.builtin);

                  let renderChips = (chips) => chips.map((chip) => html`
                    <option value=${chip.id}>${chip.name}</option>
                  `);

                  return html`
                    <optgroup label="Built-in">${renderChips(builtinChips)}</optgroup>
                    <optgroup label="User">${renderChips(userChips)}</optgroup>
                 `;
                })()}
              </select>
              ${canRevealChipsDir && html`<button type="button" class="btn" onClick=${() => {
                backend.revealChipsDirectory();
              }}>Open chip directory</button>`}
              <label for="input-showall">
                <input type="checkbox" id="input-showall" class="input-checkbox" onChange=${(event) => {
                  this.setState({
                    showAll: event.target.checked
                  });
                }} />
                <span>Show all devices</span>
              </label>
            </div>

            <ul class=${'device-list' + (this.state.loading !== null ? ' _disabled' : '')}>
              ${items && chip && items.map((device) => html`
                <li class="device-item">
                  <button type="button" class=${'device-button' + (this.state.loading === device.id ? ' _loading' : '')} onClick=${() => {
                    this.setState({ loading: device.id });
                    this.props.onSelect({ chip: this.state.chip, device }).then(() => {
                      this.setState({ loading: null });
                    })
                  }}>
                    <div class="device-name">${device.description || device.name}</div>
                    <div class="device-description">${device.id}</div>
                    <div class="device-loading">Loading</div>
                  </button>
                </li>
              `)}
              ${items && chip && items.length < 1 && html`<li class="device-item device-item--blank">No device found</li>`}
              ${items && !chip && html`<li class="device-item device-item--blank">No chip selected</li>`}
              ${!items && html`<li class="device-item device-item--blank">Loading...</li>`}
            </ul>

            <div class="actions">
              <button type="button" class="btn" onClick=${(event) => {
                this.props.onSelect(null);
              }}>Cancel</button>
              <div class="actions-group">
                ${canPrompt && html`<button type="button" class="btn" onClick=${() => {
                  backend.promptDevice(this.state.showAll ? null : this.state.chip).then((device) => {
                    if (device) {
                      this.props.onSelect({ chip: this.state.chip, device });
                    }
                  });
                }}>Add device</button>`}
                <button type="button" class="btn" onClick=${() => {
                  this.loadList({ reload: true });
                }}>Refresh</button>
              </div>
            </div>
          </div>
        </div>
      </dialog>
    `;
  }
}
