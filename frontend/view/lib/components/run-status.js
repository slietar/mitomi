import { Component, createRef } from 'preact';

import ValveList from '../components/valve-list.js';
import html from '../html.js';
import * as util from '../util.js';


export default class RunStatus extends Component {
  constructor(props) {
    super(props);

    this.progressAnimation = null;
    this.progressRef = createRef();
  }

  get status() {
    return this.props.run.status;
  }

  componentDidMount() {
    this.handleProgressAnimation();
  }

  componentDidUpdate() {
    this.handleProgressAnimation();
  }

  handleProgressAnimation() {
    if (!this.status || !this.progressRef.current.animate /* Not supported */) {
      return;
    }

    let duration = this.status.state.duration;
    duration = duration && (duration * 1000);

    if (duration && (!this.progressAnimation || this.status.progress === 0)) {
      if (this.progressAnimation) {
        this.progressAnimation.cancel();
      }

      this.progressAnimation = this.progressRef.current.animate([
        { width: '0' },
        { width: '100%' }
      ], {
        duration
      });
    }

    if (duration && !this.status.paused) {
      this.progressAnimation.play();
    } else if (this.progressAnimation) {
      this.progressAnimation.pause();
    }

    if (duration) {
      this.progressAnimation.currentTime = this.status.progress * duration
        // Correction to account for communication delay.
        + (this.status.paused ? 0 : Date.now() - this.status.resumeTime.getTime());
    } else if (this.progressAnimation) {
      this.progressAnimation.pause();
      this.progressAnimation.currentTime = 0;
    }
  }

  render() {
    let status = this.props.run.status;

    if (!status) {
      return html`<p>Loading...</p>`;
    }

    let { stage, step, state } = status;
    let now = Date.now();

    let remTimeFromResume = state.duration && state.duration * (1 - status.progress);
    let message = status.waiting
      ? 'Waiting for confirmation'
      : status.paused
        ? 'Paused'
        : util.formatRemainingTime(remTimeFromResume - (now - status.resumeTime.getTime()) / 1000);

    let endTime = state.duration
      ? util.formatTime(new Date(
          (status.paused ? now : status.resumeTime.getTime()) + remTimeFromResume * 1000
        ))
      : null;

    return html`
      <div class="step-list">
        <div class="step-status">Current step</div>
        <div class="step-item">
          <div class="step-header">
            <h3 class="step-name">${step.name}</h3>
            ${state.duration && html`<div class="step-duration">${util.formatTime(status.startTime)} – ${endTime} • ${util.formatDuration(state.duration)}</div>`}
          </div>
          ${state.confirm && html`<p class="step-description">${state.confirm}</p>`}
          <${ValveList} chip=${this.props.connection.chip} valves=${state.valves} />

          <div class=${'step-progress' + (status.paused ? ' _paused' : '') + (status.waiting ? ' _disabled' : '')}>
            <div class="step-progress-inner" ref=${this.progressRef}></div>
          </div>


          <div class="step-footer">
            <div class="step-timeleft">${message}</div>
            <div class="step-actions">
              ${!status.waiting && html` <button type="button" class="btn" onClick=${() => {
                if (status.paused) {
                  this.props.run.resume();
                } else {
                  this.props.run.pause();
                }
              }}>${status.paused ? 'Resume' : 'Pause'}</button>`}
              <button type="button" class="btn" onClick=${() => {
                this.props.run.skipState();
              }}>${status.waiting ? 'Continue' : 'Skip'}</button>
            </div>
          </div>
        </div>
      </div>
    `;
  }
}
