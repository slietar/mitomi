import { Component } from 'preact';

import ValveList from '../components/valve-list.js';
import html from '../html.js';
import * as util from '../util.js';


export default class ProtocolOverview extends Component {
  constructor(props) {
    super(props);
    this.state = {
      hidePastSteps: this.props.plan.history.length > 0,
      openSteps: new Set()
    };

    this.oldStepId = null;
  }

  render() {
    let protocol = this.props.plan.protocol;

    let status = this.props.status;
    let { history, position } = status
      ? { history: status.history,
          position: { progress: status.progress, stateIndex: status.stateIndex } }
      : this.props.plan;

    let openSteps = this.state.openSteps;
    let hidePastSteps = this.state.hidePastSteps;

    // console.log(status);

    /* let renderStages = [];
    let renderStage = null;
    let renderStep = null;

    if (true) {
      for (let entryIndex = 0; entryIndex < status.history.length; entryIndex++) {
        let entry = status.history[entryIndex];

        let stateIndex = entry.stateIndex;
        let state = protocol.states[stateIndex];
        let [stageIndex, stepIndex] = state.indices;

        // TODO: add missing steps and stages

        if (renderStage && stageIndex !== renderStage.stageIndex) {
          renderStage = null;
          renderStep = null;
        }

        if (!renderStage) {
          renderStage = {
            active: false,
            entryIndex,
            stageIndex,
            steps: []
          };
          renderStages.push(renderStage);
        }

        if (renderStep && (stepIndex !== renderStep.stepIndex || stateIndex <= renderStep.lastStateIndex)) {
          renderStep = null;
        }

        if (!renderStep) {
          renderStep = {
            active: false, // isStepActive(stageIndex, stepIndex),
            entryIndex,
            lastStateIndex: stateIndex, // Not part of the output, only used to build the structure.
            states: {},
            stepIndex
          };
          renderStage.steps.push(renderStep);
        }

        renderStep.states[stateIndex] = entry.events;
        renderStep.lastStateIndex = stateIndex;
      }
    }

    let time = Date.now();

    for (let stateIndex = status.stateIndex; stateIndex < protocol.states.length; stateIndex++) {
      let state = protocol.states[stateIndex];
      let [stageIndex, stepIndex] = state.indices;

      if (renderStage && stageIndex !== renderStage.stageIndex) {
        renderStage = null;
        renderStep = null;
      }

      if (!renderStage) {
        renderStage = {
          active: null,
          entryIndex: null,
          stageIndex,
          steps: []
        };
        renderStages.push(renderStage);
      }

      if (renderStep && (stepIndex !== renderStep.stepIndex) /* || stateIndex <= renderStep.lastStateIndex) *) {
        renderStep = null;
      }

      if (!renderStep) {
        renderStep = {
          active: false, // isStepActive(stageIndex, stepIndex),
          entryIndex: null,
          lastStateIndex: stateIndex,
          states: {},
          stepIndex
        };
        renderStage.steps.push(renderStep);
      }

      if (renderStep.states[stateIndex]) {
      // if (stateIndex === this.stateIndex) {
        time += (1 - status.progress) *  state.duration * 1000;
        renderStage.active = true;
        renderStep.active = true;
      } else {
        renderStep.states[stateIndex] = [{
          estimated: true,
          paused: false,
          progress: 0,
          time
        }];

        renderStep.lastStateIndex = stateIndex;
        time += state.duration * 1000;
      }
    } */

    let renderStages = protocol.stages.map((stage, stageIndex) => {
      let nextStage = protocol.stages[stageIndex + 1];

      return {
        active: false,
        past: nextStage && nextStage.stateIndex <= position.stateIndex,
        stageIndex,
        steps: stage.steps.map((_step, stepIndex) => {
          let nextStep = stage.steps[stepIndex + 1];

          return {
            past: nextStep && nextStep.stateIndex <= position.stateIndex,
            states: {},
            stepIndex,
            time: null
          };
        })
      }
    });

    for (let entryIndex = 0; entryIndex < history.length; entryIndex++) {
      let entry = history[entryIndex];
      let nextEntry = history[entryIndex + 1];

      let state = protocol.states[entry.stateIndex];
      let [stageIndex, stepIndex] = state.indices;

      let firstEvent = entry.events[0];
      let lastEvent = entry.events.slice(-1)[0];

      renderStages[stageIndex].steps[stepIndex].states[entry.stateIndex] = {
        active: false,
        duration: (entry.stateIndex < position.stateIndex && state.duration !== null) ? ((nextEntry ? nextEntry.events[0].time : lastEvent.time) - firstEvent.time) : null,
        time: firstEvent.time
      };
    }

    let now = Date.now();
    let time = now;

    // TODO
    if (status) {
      time = status.resumeTime.getTime();
    }

    for (let stateIndex = 0; stateIndex < protocol.states.length; stateIndex++) {
      let state = protocol.states[stateIndex];
      let [stageIndex, stepIndex] = state.indices;

      let renderStage = renderStages[stageIndex];
      let renderStep = renderStage.steps[stepIndex];
      let renderState = renderStep.states[stateIndex];

      if (!renderState) {
        renderState = {
          active: false,
          duration: null,
          time: null
        };
        renderStep.states[stateIndex] = renderState;
      }

      if (stateIndex >= position.stateIndex) {
        renderState.time = time;

        if (state.duration !== null) {
          time += state.duration * 1000 * (stateIndex === position.stateIndex ? (1 - position.progress) : 1);
        }
      }

      if (renderStep.time === null) {
        renderStep.time = renderState.time;
      }

      renderState.active = stateIndex === position.stateIndex;
      renderStep.active = renderStep.active || renderState.active;
      renderStage.active = renderStage.active || renderStep.active;
    }

    // console.log(renderStages);


    let stepCounts = {};

    for (let renderStage of renderStages) {
      for (let renderStep of renderStage.steps) {
        let partialId = renderStage.stageIndex * (1 << 20) + renderStep.stepIndex * (1 << 10);

        if (!stepCounts[partialId]) {
          stepCounts[partialId] = 0;
        }

        renderStep.id = partialId + stepCounts[partialId];
        stepCounts[partialId] += 1;

        if (renderStep.active && this.oldStepId !== renderStep.id) {
          this.oldStepId = renderStep.id;
          openSteps.add(renderStep.id);
        }
      }
    }


    return html`
      <div class="proto">
        ${renderStages.map((renderStage, renderStageIndex) => {
          let stage = protocol.stages[renderStage.stageIndex];

          if (hidePastSteps && renderStage.past) {
            return null;
          }

          return html`
            <section class="proto-stage">
              <div class="proto-stageheader">
                <h3 class="proto-stagename">${stage.name}</h3>
                ${(history.length > 0 && (renderStageIndex === 0 || (hidePastSteps && renderStage.active))) ? html`<button type="button" onClick=${() => {
                  this.setState({ hidePastSteps: !hidePastSteps });
                }}>${hidePastSteps ? 'Show' : 'Hide'} past steps</button>` : ''}
              </div>
              <div class="proto-steplist">
                ${renderStage.steps.map((renderStep) => {
                  let step = stage.steps[renderStep.stepIndex];

                  if (hidePastSteps && renderStep.past) {
                    return null;
                  }

                  return html`
                    <div class="proto-step${(openSteps.has(renderStep.id)) ? ' _open' : ''}${renderStep.active ? ' _active' : ''}">
                      <button type="button" class="proto-stepheader" onClick=${() => {
                        if (openSteps.has(renderStep.id)) {
                          openSteps.delete(renderStep.id);
                        } else {
                          openSteps.add(renderStep.id);
                        }

                        this.setState({ openSteps });
                      }}>
                        <div class="proto-step-name">${step.name}<!--  / ${renderStep.id} --></div>
                        <div class="proto-step-time">${util.formatTime(renderStep.time)}</div>
                      </button>
                      <div class="proto-stepcontents">
                        ${this.renderActions([step.action], renderStep)}
                      </div>
                    </div>
                  `;
                })}
              </div>
            </section>
          `;
        })}
      </div>`;
  }

  renderActions(actions, renderStep) {
    return html`
      <div class="proto-step-substep-list">
        ${actions.map((action) => {
          let stateIndex = action.stateIndex;

          if (action.type === 'actions') {
            return html`
              <div class="proto-step-substep-call">
                <div class="proto-step-substep-name">${html([util.formatCode(action.name)])}</div>
                ${this.renderActions(action.actions, renderStep)}
              </div>
            `;
          }

          if (action.type === 'state') {
            let state = this.props.plan.protocol.states[stateIndex];
            let renderState = renderStep.states[stateIndex];

            let disabled = false;
            let duration = renderState.duration !== null
              ? renderState.duration
              : state.duration !== null
                ? state.duration * 1000
                : null;

            let note = [];

            if (duration !== null) {
              note.push(util.formatDuration(duration / 1000));
            } else if (state.duration === null) {
              note.push('Confirmation');
            }

            if (renderState.time !== null) {
              note.push(util.formatTime(renderState.time));
            } else {
              note[0] += ' (skipped)';
            }

            return html`
              <button class=${'proto-step-substep-state' + (renderState.active ? ' _active' : '') + (disabled ? ' _disabled' : '')} onClick=${() => {
                this.props.onSelectState(stateIndex);
              }}>
                <${ValveList} chip=${this.props.connection.chip} valves=${state.visible} />
                ${note.length > 0 && html`<div class="proto-step-substep-time">${note.join(' • ')}</div>`}
              </button>
            `;
          }
        })}
      </div>
    `;
  }
}
