import { Component } from 'preact';

import html from '../html.js';


export default class Header extends Component {
  render() {
    let connection = this.props.connection;

    return html`
      <header>
        <h1>${this.props.title}</h1>

        ${connection ? html`
          <div class="devicestatus devicestatus--on">
            ${connection.device.description || connection.device.name}<!-- / ${connection.chip.name} -->
          </div>
          <button type="button" class="btn" onClick=${(event) => {
            connection.close();
          }}>Disconnect</button>
        ` : html`
          <div class="devicestatus devicestatus--off">Not connected</div>
          <button type="button" class="btn" onClick=${(event) => {
            this.props.app.openSelectionDialog();
          }}>Connect</button>
        `}
      </header>
    `;
  }
}
