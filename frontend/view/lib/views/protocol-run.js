import { Component } from 'preact';

import html from '../html.js';
import ProtocolOverview from '../components/protocol-overview.js';
import RunStatus from '../components/run-status.js';
import * as util from '../util.js';


export default class ViewProtocolRun extends Component {
  constructor(props) {
    super(props);

    this.state = {
      plan: null,
      run: null,
      time: Date.now()
    };

    this.interval = null;
    this.run = null;
  }

  get connection() {
    return this.props.connection;
  }

  get protocol() {
    return this.state.plan && this.state.plan.protocol;
  }

  componentDidMount() {
    this.interval = setInterval(() => {
      this.setState({ time: Date.now() });
    }, 15e3);
  }

  componentWillUnmount() {
    if (this.interval !== null) {
      clearInterval(this.interval);
    }
  }

  componentDidUpdate(prevProps) {
    if (this.connection && prevProps.connection !== this.connection) {
      this.connection.on('close', () => {
        if (this.state.plan) {
          this.setState({ plan: null });
        }
      });

      // debug
      return;
      this.connection.promptProtocol().then((plan) => {
        this.setState({ plan });

        setTimeout(() => {
          // this.startProtocol('CHIP1');
        }, 100);
      });
    }
  }

  startProtocol(chipId) {
    this.connection.startProtocol({
      chipInfo: { id: chipId },
    }, (status) => {
      if (!status) {
        // The run was cancelled.
        this.setState({ run: null });
        this.run = null;

        return;
      }

      this.setState({
        run: this.run
      });
    }).then((run) => {
      this.run = run;
      this.setState({ run });
    });
  }

  reloadProtocol() {
    this.connection.reloadProtocol().then((plan) => {
      this.run = null;

      if (plan) {
        this.setState({ plan });
      } else {
        this.setState({ plan: null });
      }
    }, (err) => {
      console.error(err);
      alert(err);
    });
  }

  render() {
    let duration = this.state.plan && this.state.plan.computeDuration();

    return html`
      ${!this.connection && html`<div class="blank">
        <h2>No device connected</h2>
        <button type="button" class="btn" onClick=${() => {
          this.props.app.openSelectionDialog();
        }}>Connect a device</button>
      </div>`}

      ${this.connection && !this.state.plan && html`<div class="blank">
        <h2>No protocol running</h2>
        <div class="blank-buttons">
          <button type="button" class="btn" onClick=${() => {
            this.props.connection.promptProtocol().then((plan) => {
              if (plan) {
                this.setState({ plan });
              }
            }, (err) => {
              console.error(err);
              alert(err.message);
            });
          }}>Load protocol</button>
          <button type="button" class="btn" onClick=${() => {
            this.props.connection.restoreProtocol().then((plan) => {
              if (plan) {
                this.setState({ plan });
              }
            }, (err) => {
              console.error(err);
              alert(err.message);
            });
          }}>Restore</button>
        </div>
      </div>`}

      ${this.connection && this.state.plan && !this.state.run && html`<div class="setup">
        <div class="setup-header">
          <h2>Overview of ${this.protocol.name}</h2>
          <button type="button" class="btn" onClick=${() => {
            this.reloadProtocol();
          }}>Reload</button>
        </div>

        ${this.protocol.warnings.length > 0 && html`<ul class="setup-warnings">
          ${this.protocol.warnings.map((warning) => {
            let text = warning.split('\'').map((segment, index) => index % 2 > 0 ? html`<code>${segment}</code>` : segment);
            return html`<li><strong>Warning</strong> ${text}</li>`;
          })}
        </ul>`}

        <${ProtocolOverview} connection=${this.connection} plan=${this.state.plan} onSelectState=${(stateIndex) => {
          this.props.connection.updateProtocol({ progress: 0, stateIndex }).then((plan) => {
            this.setState({ plan });
          }, (err) => {
            console.error(err);
            alert(err.message);
          });
        }} />

        <form onSubmit=${(event) => {
          event.preventDefault();

          this.setState({ time: Date.now() });

          let data = new FormData(event.target);
          let chipId = data.get('chip-id');

          this.startProtocol(chipId);
          document.body.scrollTop = 0;
        }}>
          <div class="setup-header">
            <h2>Metadata</h2>
          </div>

          <div class="form-control">
            <label>Chip ID</label>
            <input type="text" name="chip-id" value=${this.state.plan.chipInfo ? this.state.plan.chipInfo.id : '' } autocomplete="off" class="input input--monospace" />
          </div>

          <div class="actions actions--block">
            <div class="status">ETA: ${util.formatTime(this.state.time + duration)} (${util.formatDuration(duration / 1000)})</div>
            <div class="actions-group">
              <button type="button" class="btn" onClick=${() => {
                this.setState({ plan: null });
              }}>Cancel</button>
              <button type="submit" class="btn">${this.state.plan.history.length > 0 ? 'Resume' : 'Start'} protocol</button>
            </div>
          </div>
        </form>
      </div>`}


      ${this.state.run && html`<div class="run">
        <${RunStatus} connection=${this.connection} run=${this.state.run} time=${this.state.time} />
        <div class="run-protocol">
          <div class="run-protocol-header">
            <h2>${this.protocol.name}</h2>
            <div class="actions-group">
              <button type="button" class="btn" onClick=${() => {
                this.reloadProtocol();
              }}>Reload</button>
              <button type="button" class="btn" onClick=${() => {

              }}>Stop</button>
            </div>
          </div>
          <${ProtocolOverview} connection=${this.connection} plan=${this.state.plan} status=${this.state.run.status} onSelectState=${(stateIndex) => {
            this.state.run.skipState({ progress: 0, stateIndex });
          }} />
        </div>
      </div>`}
    `;
  }
}
