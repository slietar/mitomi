import { Component } from 'preact';

import { Controller } from '../components/controller.js';
import html from '../html.js';


export default class ViewManualControl extends Component {
  render() {
    return html`
      ${this.props.connection
        ? html`<${Controller} connection=${this.props.connection} />`
        : html`<div class="blank">
          <h2>No device connected</h2>
          <button type="button" class="btn" onClick=${() => {
            this.props.app.openSelectionDialog();
          }}>Connect a device</button>
        </div>`}
    `;
  }
}
