import * as yaml from 'js-yaml';

import EventEmitter from './common/event-emitter.js';
import Plan from './common/plan.js';


export default class MockBackend {
  constructor() {
    this._chips = null;

    this.connection = null;
  }


  async initialize() {
    this._chips = await Promise.all([
      'data/chips/mitomi1024.yml'
    ].map(async (filepath) => {
      let chipUrl = new URL(filepath, location);
      let chip = yaml.load(await (await fetch(filepath)).text());

      if (chip.schematic) {
        chip.schematic = await (await fetch(new URL(chip.schematic, chipUrl))).text();
      }

      return chip;
    }));
  }


  async listChips(options = {}) {
    return this._chips;
  }

  async listDevices(options = {}) {
    return [{
      id: 'fake',
      name: 'Fake device',

      supported: true
    }];
  }


  async connect(chip, device) {
    if (this.connection) {
      throw new Error('Cannot open a second connection');
    }

    let connection = new Connection(this);
    this.connection = connection;

    await new Promise((resolve) => void setTimeout(() => void resolve(), 200));

    await connection.open(chip, device);

    connection.on('close', () => {
      connection = null;
    });

    return connection;
  }

  async disconnect() {
    await this.connection.close();
    this.connection = null;
  }


  static isSupported() {
    return true;
  }
}


class Connection extends EventEmitter {
  constructor(backend) {
    super();

    this._backend = backend;

    this.chip = null;
    this.device = null;

    this._state = null;
    this._plan = null;
    this._run = null;
  }

  get _protocol() {
    return this._plan && this._plan.protocol;
  }

  async open(chip, device) {
    this.chip = chip;
    this.device = device;

    this._state = 0;
  }

  async close() {
    this.emit('close', false);
  }


  async readAll() {
    return this._state;
  }

  async writeAll(state) {
    this._state = state;
  }


  async startProtocol(options, progressCallback) {
    if (options.chipInfo) {
      this._plan.chipInfo = options.chipInfo;
    }

    let history = this._plan.history;
    let historyItem = null;
    let resumeTime = null;
    let stateIndex = this._plan.position.stateIndex;
    let timeout = null;

    let run = {
      status: null,

      async pause() {
        if (this.status.paused) {
          console.warn('Run already paused');
          return;
        }

        stopTimer();

        let time = Date.now();
        let progress = getProgress(time);

        Object.assign(this.status, {
          paused: true,
          progress
        });

        historyItem.events.push({ paused: true, progress, time });
        update();
      },
      async resume() {
        if (!this.status.paused) {
          console.warn('Run already running');
          return;
        }

        let time = new Date();
        resumeTime = time;

        Object.assign(this.status, {
          paused: false,
          resumeTime
        });

        historyItem.events.push({ paused: false, progress: this.status.progress, time: time.getTime() })
        update();

        timeout = setTimeout(() => {
          timeout = null;
          endState();
        }, (1 - this.status.progress) * this.status.state.duration * 1000);
      },

      async skipState(position) {
        stopTimer();
        endState(position);
      },
      async stop() {
        stopTimer();

        this._run = null;
        progressCallback(null);
      }
    };

    let getProgress = (time = Date.now()) => {
      return run.status.state.duration !== null
        ? run.status.progress + (time - resumeTime.getTime()) / (run.status.state.duration * 1000)
        : 0;
    };

    let update = () => {
      progressCallback(run.status);
    };

    let stopTimer = () => {
      if (timeout !== null) {
        clearTimeout(timeout);
        timeout = null;
      }
    };

    let startState = (progress = 0) => {
      let state = this._protocol.states[stateIndex];
      let stage = this._protocol.stages[state.indices[0]];
      let step = stage.steps[state.indices[1]];

      resumeTime = new Date();

      let paused = run.status
        ? run.status.paused
        : false;

      if (!(historyItem && historyItem.stateIndex === stateIndex)) {
        historyItem = {
          stateIndex,
          events: []
        };

        history.push(historyItem);
      }

      historyItem.events.push({
        paused,
        progress,
        time: Date.now()
      });

      run.status = {
        history,

        stage,
        step,
        state,
        stateIndex,

        progress,
        resumeTime,
        startTime: new Date(),

        paused,
        waiting: state.duration === null,

        position: {
          progress,
          stateIndex
        }
      };

      update();

      if (!run.status.paused && !run.status.waiting) {
        timeout = setTimeout(() => {
          timeout = null;
          endState();
        }, (1 - progress) * state.duration * 1000);
      }
    };

    let endState = (position = { progress: 0, stateIndex: stateIndex + 1 }) => {
      let time = Date.now();

      historyItem.events.push({
        paused: run.status.paused,
        progress: getProgress(time),
        time
      });

      if (position.stateIndex < this._protocol.states.length) {
        stateIndex = position.stateIndex;
        startState(position.progress);
      } else {
        // Stop.
      }
    };

    queueMicrotask(() => {
      startState(this._plan.position.progress);
    });

    this._run = run;
    return run;
  }

  async promptProtocol() {
    let data = {"name": "Protocol 6", "stages": [{"name": "Stage #1", "stateIndex": 0, "steps": [{"name": "Step #1", "action": {"type": "actions", "actions": [{"type": "state", "stateIndex": 0}, {"type": "state", "stateIndex": 1}, {"type": "actions", "actions": [{"type": "state", "stateIndex": 2}, {"type": "state", "stateIndex": 3}], "name": "Call to {seq()}"}, {"type": "actions", "actions": [{"type": "state", "stateIndex": 4}, {"type": "state", "stateIndex": 5}], "name": "Call to {seq()}"}, {"type": "state", "stateIndex": 6}, {"type": "state", "stateIndex": 7}, {"type": "state", "stateIndex": 8}, {"type": "state", "stateIndex": 9}], "name": "Call to {func()}"}, "stateIndex": 0}, {"name": "Step #2", "action": {"type": "actions", "actions": [{"type": "state", "stateIndex": 10}, {"type": "state", "stateIndex": 11}, {"type": "actions", "actions": [{"type": "state", "stateIndex": 12}, {"type": "state", "stateIndex": 13}], "name": "Call to {seq()}"}, {"type": "actions", "actions": [{"type": "state", "stateIndex": 14}, {"type": "state", "stateIndex": 15}], "name": "Call to {seq()}"}, {"type": "state", "stateIndex": 16}, {"type": "state", "stateIndex": 17}, {"type": "state", "stateIndex": 18}, {"type": "state", "stateIndex": 19}], "name": "Call to {func()}"}, "stateIndex": 10}]}, {"name": "Stage #2", "stateIndex": 20, "steps": [{"name": "Step #1", "action": {"type": "actions", "actions": [{"type": "state", "stateIndex": 20}, {"type": "state", "stateIndex": 21}, {"type": "actions", "actions": [{"type": "state", "stateIndex": 22}, {"type": "state", "stateIndex": 23}], "name": "Call to {seq()}"}, {"type": "actions", "actions": [{"type": "state", "stateIndex": 24}, {"type": "state", "stateIndex": 25}], "name": "Call to {seq()}"}, {"type": "state", "stateIndex": 26}, {"type": "state", "stateIndex": 27}, {"type": "state", "stateIndex": 28}, {"type": "state", "stateIndex": 29}], "name": "Call to {func()}"}, "stateIndex": 20}, {"name": "Step #2", "action": {"type": "actions", "actions": [{"type": "state", "stateIndex": 30}, {"type": "state", "stateIndex": 31}, {"type": "actions", "actions": [{"type": "state", "stateIndex": 32}, {"type": "state", "stateIndex": 33}], "name": "Call to {seq()}"}, {"type": "actions", "actions": [{"type": "state", "stateIndex": 34}, {"type": "state", "stateIndex": 35}], "name": "Call to {seq()}"}, {"type": "state", "stateIndex": 36}, {"type": "state", "stateIndex": 37}, {"type": "state", "stateIndex": 38}, {"type": "state", "stateIndex": 39}], "name": "Call to {func()}"}, "stateIndex": 30}]}, {"name": "Stage #3", "stateIndex": 40, "steps": [{"name": "Step #1", "action": {"type": "actions", "actions": [{"type": "state", "stateIndex": 40}, {"type": "state", "stateIndex": 41}, {"type": "actions", "actions": [{"type": "state", "stateIndex": 42}, {"type": "state", "stateIndex": 43}], "name": "Call to {seq()}"}, {"type": "actions", "actions": [{"type": "state", "stateIndex": 44}, {"type": "state", "stateIndex": 45}], "name": "Call to {seq()}"}, {"type": "state", "stateIndex": 46}, {"type": "state", "stateIndex": 47}, {"type": "state", "stateIndex": 48}, {"type": "state", "stateIndex": 49}], "name": "Call to {func()}"}, "stateIndex": 40}, {"name": "Step #2", "action": {"type": "actions", "actions": [{"type": "state", "stateIndex": 50}, {"type": "state", "stateIndex": 51}, {"type": "actions", "actions": [{"type": "state", "stateIndex": 52}, {"type": "state", "stateIndex": 53}], "name": "Call to {seq()}"}, {"type": "actions", "actions": [{"type": "state", "stateIndex": 54}, {"type": "state", "stateIndex": 55}], "name": "Call to {seq()}"}, {"type": "state", "stateIndex": 56}, {"type": "state", "stateIndex": 57}, {"type": "state", "stateIndex": 58}, {"type": "state", "stateIndex": 59}], "name": "Call to {func()}"}, "stateIndex": 50}]}], "states": [{"confirm": null, "duration": 10.0, "indices": [0, 0, 0], "valves": [11, 12], "visible": [12]}, {"confirm": null, "duration": 10.0, "indices": [0, 0, 1], "valves": [11, 13], "visible": [13]}, {"confirm": null, "duration": null, "indices": [0, 0, 2, 0], "valves": [11, 12], "visible": [12]}, {"confirm": null, "duration": null, "indices": [0, 0, 2, 1], "valves": [11], "visible": []}, {"confirm": null, "duration": null, "indices": [0, 0, 3, 0], "valves": [11, 12], "visible": [12]}, {"confirm": null, "duration": null, "indices": [0, 0, 3, 1], "valves": [11], "visible": []}, {"confirm": "Xx", "duration": null, "indices": [0, 0, 4], "valves": [11], "visible": []}, {"confirm": null, "duration": null, "indices": [0, 0, 5], "valves": [0, 1, 2, 3, 4, 5, 6, 7, 11, 12, 13, 14, 15, 16, 17, 18], "visible": [0, 1, 2, 3, 4, 5, 6, 7, 12, 13, 14, 15, 16, 17, 18]}, {"confirm": null, "duration": 10.0, "indices": [0, 0, 6], "valves": [11, 14], "visible": [14]}, {"confirm": null, "duration": 10.0, "indices": [0, 0, 7], "valves": [11, 15], "visible": [15]}, {"confirm": null, "duration": 10.0, "indices": [0, 1, 0], "valves": [11, 12], "visible": [12]}, {"confirm": null, "duration": 10.0, "indices": [0, 1, 1], "valves": [11, 13], "visible": [13]}, {"confirm": null, "duration": null, "indices": [0, 1, 2, 0], "valves": [11, 12], "visible": [12]}, {"confirm": null, "duration": null, "indices": [0, 1, 2, 1], "valves": [11], "visible": []}, {"confirm": null, "duration": null, "indices": [0, 1, 3, 0], "valves": [11, 12], "visible": [12]}, {"confirm": null, "duration": null, "indices": [0, 1, 3, 1], "valves": [11], "visible": []}, {"confirm": "Xx", "duration": null, "indices": [0, 1, 4], "valves": [11], "visible": []}, {"confirm": null, "duration": null, "indices": [0, 1, 5], "valves": [0, 1, 2, 3, 4, 5, 6, 7, 11, 12, 13, 14, 15, 16, 17, 18], "visible": [0, 1, 2, 3, 4, 5, 6, 7, 12, 13, 14, 15, 16, 17, 18]}, {"confirm": null, "duration": 10.0, "indices": [0, 1, 6], "valves": [11, 14], "visible": [14]}, {"confirm": null, "duration": 10.0, "indices": [0, 1, 7], "valves": [11, 15], "visible": [15]}, {"confirm": null, "duration": 10.0, "indices": [1, 0, 0], "valves": [11, 12], "visible": [12]}, {"confirm": null, "duration": 10.0, "indices": [1, 0, 1], "valves": [11, 13], "visible": [13]}, {"confirm": null, "duration": null, "indices": [1, 0, 2, 0], "valves": [11, 12], "visible": [12]}, {"confirm": null, "duration": null, "indices": [1, 0, 2, 1], "valves": [11], "visible": []}, {"confirm": null, "duration": null, "indices": [1, 0, 3, 0], "valves": [11, 12], "visible": [12]}, {"confirm": null, "duration": null, "indices": [1, 0, 3, 1], "valves": [11], "visible": []}, {"confirm": "Xx", "duration": null, "indices": [1, 0, 4], "valves": [11], "visible": []}, {"confirm": null, "duration": null, "indices": [1, 0, 5], "valves": [0, 1, 2, 3, 4, 5, 6, 7, 11, 12, 13, 14, 15, 16, 17, 18], "visible": [0, 1, 2, 3, 4, 5, 6, 7, 12, 13, 14, 15, 16, 17, 18]}, {"confirm": null, "duration": 10.0, "indices": [1, 0, 6], "valves": [11, 14], "visible": [14]}, {"confirm": null, "duration": 10.0, "indices": [1, 0, 7], "valves": [11, 15], "visible": [15]}, {"confirm": null, "duration": 10.0, "indices": [1, 1, 0], "valves": [11, 12], "visible": [12]}, {"confirm": null, "duration": 10.0, "indices": [1, 1, 1], "valves": [11, 13], "visible": [13]}, {"confirm": null, "duration": null, "indices": [1, 1, 2, 0], "valves": [11, 12], "visible": [12]}, {"confirm": null, "duration": null, "indices": [1, 1, 2, 1], "valves": [11], "visible": []}, {"confirm": null, "duration": null, "indices": [1, 1, 3, 0], "valves": [11, 12], "visible": [12]}, {"confirm": null, "duration": null, "indices": [1, 1, 3, 1], "valves": [11], "visible": []}, {"confirm": "Xx", "duration": null, "indices": [1, 1, 4], "valves": [11], "visible": []}, {"confirm": null, "duration": null, "indices": [1, 1, 5], "valves": [0, 1, 2, 3, 4, 5, 6, 7, 11, 12, 13, 14, 15, 16, 17, 18], "visible": [0, 1, 2, 3, 4, 5, 6, 7, 12, 13, 14, 15, 16, 17, 18]}, {"confirm": null, "duration": 10.0, "indices": [1, 1, 6], "valves": [11, 14], "visible": [14]}, {"confirm": null, "duration": 10.0, "indices": [1, 1, 7], "valves": [11, 15], "visible": [15]}, {"confirm": null, "duration": 10.0, "indices": [2, 0, 0], "valves": [11, 12], "visible": [12]}, {"confirm": null, "duration": 10.0, "indices": [2, 0, 1], "valves": [11, 13], "visible": [13]}, {"confirm": null, "duration": null, "indices": [2, 0, 2, 0], "valves": [11, 12], "visible": [12]}, {"confirm": null, "duration": null, "indices": [2, 0, 2, 1], "valves": [11], "visible": []}, {"confirm": null, "duration": null, "indices": [2, 0, 3, 0], "valves": [11, 12], "visible": [12]}, {"confirm": null, "duration": null, "indices": [2, 0, 3, 1], "valves": [11], "visible": []}, {"confirm": "Xx", "duration": null, "indices": [2, 0, 4], "valves": [11], "visible": []}, {"confirm": null, "duration": null, "indices": [2, 0, 5], "valves": [0, 1, 2, 3, 4, 5, 6, 7, 11, 12, 13, 14, 15, 16, 17, 18], "visible": [0, 1, 2, 3, 4, 5, 6, 7, 12, 13, 14, 15, 16, 17, 18]}, {"confirm": null, "duration": 10.0, "indices": [2, 0, 6], "valves": [11, 14], "visible": [14]}, {"confirm": null, "duration": 10.0, "indices": [2, 0, 7], "valves": [11, 15], "visible": [15]}, {"confirm": null, "duration": 10.0, "indices": [2, 1, 0], "valves": [11, 12], "visible": [12]}, {"confirm": null, "duration": 10.0, "indices": [2, 1, 1], "valves": [11, 13], "visible": [13]}, {"confirm": null, "duration": null, "indices": [2, 1, 2, 0], "valves": [11, 12], "visible": [12]}, {"confirm": null, "duration": null, "indices": [2, 1, 2, 1], "valves": [11], "visible": []}, {"confirm": null, "duration": null, "indices": [2, 1, 3, 0], "valves": [11, 12], "visible": [12]}, {"confirm": null, "duration": null, "indices": [2, 1, 3, 1], "valves": [11], "visible": []}, {"confirm": "Xx", "duration": null, "indices": [2, 1, 4], "valves": [11], "visible": []}, {"confirm": null, "duration": null, "indices": [2, 1, 5], "valves": [0, 1, 2, 3, 4, 5, 6, 7, 11, 12, 13, 14, 15, 16, 17, 18], "visible": [0, 1, 2, 3, 4, 5, 6, 7, 12, 13, 14, 15, 16, 17, 18]}, {"confirm": null, "duration": 10.0, "indices": [2, 1, 6], "valves": [11, 14], "visible": [14]}, {"confirm": null, "duration": 10.0, "indices": [2, 1, 7], "valves": [11, 15], "visible": [15]}], "warnings": []}

    this._plan = new Plan({
      chipInfo: null,
      history: [],
      protocol: data,
      position: {
        progress: 0,
        stateIndex: 0
      }
    });

    return this._plan;
  }

  async reloadProtocol() {
    if (!this._run) {
      return this._plan;
    }

    if (!this._run.status.paused) {
      this._run.pause();
    }

    // await new Promise((r) => void setTimeout(() => void r(), 1000));

    this._run.stop();

    let status = this._run.status;
    Object.assign(this._plan, {
      history: status.history,
      position: { progress: status.progress, stateIndex: status.stateIndex }
    });

    return this._plan;
  }

  async updateProtocol(position) {
    if (!this._plan) {
      throw new Error('No protocol');
    }

    this._plan.position = position;
    return this._plan;
  }
}
