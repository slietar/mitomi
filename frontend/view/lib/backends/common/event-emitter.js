export default class EventEmitter {
  constructor() {
    this._listeners = {};
  }

  on(name, listener) {
    if (!(name in this._listeners)) {
      this._listeners[name] = [];
    }

    this._listeners[name].push(listener);

    return this;
  }

  emit(name, event) {
    let listeners = this._listeners[name];

    if (listeners) {
      for (let listener of listeners) {
        listener.call(this, event);
      }
    }
  }
}
