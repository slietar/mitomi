export default class Plan {
  constructor(plan) {
    Object.assign(this, plan);
  }

  computeDuration(status = this.status) {
    return this.protocol.states.reduce((sum, state, stateIndex) => {
      let duration = (state.duration || 0) * 1000;

      if (status) {
        if (stateIndex < status.stateIndex) {
          return sum;
        } else if (stateIndex === status.stateIndex) {
          return sum + duration * (1 - status.progress);
        }
      }

      return sum + duration;
    }, 0);
  }

  computeStateTimes(startTime, status = this.status) {
    let currentTime = startTime;

    return this.protocol.states.map((state, stateIndex) => {
      let duration = (state.duration || 0) * 1000;

      if (status && stateIndex === status.stateIndex) {
        currentTime += duration * (1 - status.progress);
      }

      if (status && stateIndex <= status.stateIndex) {
        let past = status.history.find((status) => status.stateIndex === stateIndex);

        if (past) {
          return past.time * 1000;
        } else {
          return null;
        }
      } else {
        let stateTime = currentTime;
        currentTime += duration;
        return stateTime;
      }
    });
  }
}
