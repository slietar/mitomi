// Legacy.
export default class Protocol {
  constructor(data, chip) {
    Object.assign(this, data);

    this.chip = chip;
  }
}
