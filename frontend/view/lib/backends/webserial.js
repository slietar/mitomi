import * as yaml from 'js-yaml';

import EventEmitter from './common/event-emitter.js';
import Protocol from './common/protocol.js';


export default class WebserialBackend {
  constructor() {
    this._chips = null;
    this._devices = null;
    this._nextDeviceId = 0;

    this.connection = null;
  }

  _createDevice(port) {
    let info = port.getInfo();
    let id = this._nextDeviceId++;

    let device = {
      id: `webserial-${id}`,
      name: `Device ${id}`,
      description: null,
      product_id: info.usbProductId,
      vendor_id: info.usbVendorId,

      _port: port
    };

    this._devices.push(device);

    return device;
  }


  async initialize() {
    this._chips = await Promise.all([
      'data/chips/mitomi1024.yml'
    ].map(async (filepath) => {
      let chipUrl = new URL(filepath, location);
      let chip = yaml.load(await (await fetch(filepath)).text());

      if (chip.schematic) {
        chip.schematic = await (await fetch(new URL(chip.schematic, chipUrl))).text();
      }

      return chip;
    }));
  }


  async listChips(options = {}) {
    return this._chips;
  }

  async listDevices(options = {}) {
    if (!this._devices || options.reload) {
      this._devices = [];
      this._nextDeviceId = 0;

      let ports = await navigator.serial.getPorts();

      for (let port of ports) {
        this._createDevice(port);
      }
    }

    return this._devices;
  }

  async promptDevice(chip) {
    let port;

    try {
      port = await navigator.serial.requestPort(chip ? {
        filters: [{
          usbProductId: chip.device.product_id,
          usbVendorId: chip.device.vendor_id
        }]
      } : {});
    } catch (err) {
      return null;
    }

    return this._createDevice(port);
  }


  async connect(chip, device) {
    if (this.connection) {
      throw new Error('Cannot open a second connection');
    }

    let connection = new Connection(this);

    await connection.open(chip, device);

    this.connection = connection;
    this.connection.on('close', () => {
      this.connection = null;
    });

    return this.connection;
  }

  async disconnect() {
    await this.connection.close();
    this.connection = null;
  }


  static isSupported() {
    return 'serial' in navigator;
  }
}


export class Connection extends EventEmitter {
  constructor(backend) {
    super();

    this._backend = backend;
    this._protocol = null;

    this._closeStreams = null;
    this._reader = null;
    this._writer = null;
    this._serialListener = null;

    this.chip = null;
    this.device = null;
  }

  async open(chip, device) {
    this.chip = chip;
    this.device = device;

    await device._port.open({ baudRate: 9600 });

    let textDecoder = new TextDecoderStream();
    let readableClosed = device._port.readable.pipeTo(textDecoder.writable);
    this._reader = textDecoder.readable.pipeThrough(new TransformStream(new LineBreakTransformer())).getReader();

    let textEncoder = new TextEncoderStream();
    let writableClosed = textEncoder.readable.pipeTo(device._port.writable);
    this._writer = textEncoder.writable.getWriter();

    this._closeStreams = async () => {
      this._reader.cancel();
      await readableClosed.catch(() => {});
      this._reader.releaseLock();

      this._writer.close();
      await writableClosed;
    };

    if (await this._getVersion() !== 8) {
      throw new Error('Unknown version');
    }

    this._serialListener = (event) => {
      if (event.target === this.device._port) {
        navigator.serial.removeEventListener('disconnect', this._serialListener);
        this.emit('close', true);
      }
    };

    navigator.serial.addEventListener('disconnect', this._serialListener);
  }

  async close() {
    navigator.serial.removeEventListener('disconnect', this._serialListener);

    await this._closeStreams();
    await this.device._port.close();

    this._closeStreams = null;
    this._reader = null;
    this._writer = null;

    this.emit('close', false);
  }


  async read(valveIndex) {
    return this.states[valveIndex]; // !!
  }

  async write(valveIndex, state) {
    let valve = this.chip.valves[valveIndex];
    await this._order(`relay ${state ? 'on' : 'off'} ${getRelayNumber(valve.channel)}`);
  }

  async readAll() {
    let value = parseInt(await this._request('relay readall'), 16);
    return this.chip.valves.map((valve) => (value & (1 << valve.channel)) > 0);
  }

  async writeAll(states) {
    let value = 0;

    for (let valveIndex = 0; valveIndex < states.length; valveIndex++) {
      let relayIndex = this.chip.valves[valveIndex].channel;
      value |= ((states[valveIndex] ? 1 : 0) << relayIndex);
    }

    await this._order(`relay writeall ${value.toString(16).padStart(8, '0')}`);
  }


  async promptProtocol() {
    /* let handles;

    try {
      handles = await window.showOpenFilePicker({
        excludeAcceptAllOption: true,
        types: [{
          description: 'Protocol Definition Files',
          accept: {
            'text/yaml': ['.yml', '.yaml']
          }
        }]
      });
    } catch (err) {
      return null;
    }

    let handle = handles[0];

    if (!handle) {
      return null;
    }

    let file = await handle.getFile();
    let text = await file.text();
    let data = yaml.load(text);

    this._protocol = processProtocol(data);

    return this._protocol; */

    let data = {"name": "Protocol 1", "aliases": {"reag1": {"aliases": [], "params": [], "valves": [0, 1, 2]}, "reag2": {"aliases": [], "params": [], "valves": [3]}}, "defaults": {"aliases": ["reag1"], "params": [], "valves": [2]}, "functions": {"seq1": {"actions": [{"type": "state", "confirm": null, "duration": {"type": "param", "param": 1}, "valves": {"aliases": [], "params": [0], "valves": []}}, {"type": "state", "confirm": null, "duration": {"type": "value", "value": 120.0}, "valves": {"aliases": [], "params": [], "valves": [3]}}, {"type": "state", "confirm": null, "duration": {"type": "param", "param": 1}, "valves": {"aliases": [], "params": [0], "valves": [4]}}], "name": "Sequence 1", "parameters": ["valve", "duration"]}, "seq2": {"actions": [{"type": "call", "arguments": [{"aliases": [], "params": [0, 1], "valves": []}, {"type": "value", "value": 5280.0}], "callee": "seq1"}, {"type": "state", "confirm": null, "duration": {"type": "value", "value": 300.0}, "valves": {"aliases": [], "params": [], "valves": [8]}}], "name": null, "parameters": ["valve", "valve"]}}, "stages": [{"name": "Stage #1", "steps": [{"action": {"type": "state", "confirm": null, "duration": {"type": "value", "value": 180.0}, "valves": {"aliases": ["reag1"], "params": [], "valves": []}}, "name": "Step #1"}, {"action": {"type": "state", "confirm": null, "duration": {"type": "value", "value": 300.0}, "valves": {"aliases": ["reag2"], "params": [], "valves": []}}, "name": "Step #2"}, {"action": {"type": "call", "arguments": [{"aliases": ["reag1"], "params": [], "valves": []}, {"type": "value", "value": 8700.0}], "callee": "seq1"}, "name": "Seq 1 on reag1"}, {"action": {"type": "call", "arguments": [{"aliases": ["reag2"], "params": [], "valves": [7]}, {"type": "value", "value": 2100.0}], "callee": "seq1"}, "name": "Seq 1 on reag2 and C8"}]}, {"name": "Stage #2", "steps": [{"action": {"type": "call", "arguments": [{"aliases": ["reag1"], "params": [], "valves": []}, {"aliases": ["reag2"], "params": [], "valves": []}], "callee": "seq2"}, "name": "Call of seq2"}]}]}


    return new Protocol(data, this.chip);
  }


  async _getVersion() {
    return parseInt(await this._request('ver'), 10);
  }

  async _order(text) {
    this._writer.write(text + '\r');
    await this._reader.read();
  }

  async _request(text) {
    this._order(text);

    let { value, done } = await this._reader.read();

    return value;
  }
}








function getRelayNumber(index) {
  return index < 10
    ? index.toString()
    : String.fromCharCode(0x41 + (index - 10));
}


class LineBreakTransformer {
  constructor() {
    this.chunks = '';
  }

  transform(chunk, controller) {
    this.chunks += chunk;

    let  lines = this.chunks.split("\n");
    this.chunks = lines.pop();

    for (let line of lines) {
      controller.enqueue(line);
    }
  }

  flush(controller) {
    controller.enqueue(this.chunks);
  }
}


function processProtocol(data) {
  return {
    chips: data.chips || null,
    name: data.name,
    stages: data.stages.map((stage, stageIndex) => ({
      name: stage.name || `Stage #${stageIndex + 1}`,
      steps: stage.steps.map((step, stepIndex) => ({
        name: step.name || `Step #${stepIndex + 1}`,
        duration: step.duration !== void 0
          ? parseDuration(step.duration)
          : null
      }))
    }))
  };
}


const TimeFactors = Object.entries({
  1: ['sec', 'second', 'seconds'],
  60: ['min', 'minute', 'minutes'],
  3600: ['hr', 'hrs', 'hour', 'hours']
});

function parseDuration(input) {
  if (typeof input === 'number') {
    return input;
  }

  let output = 0;
  let words = input.split(' ');

  for (let index = 0; index < words.length; index += 2) {
    let value = parseFloat(words[index]);
    let keyword = words[index + 1];

    if (keyword) {
      let factor = TimeFactors.find(([_factor, keywords]) => keywords.includes(keyword));

      if (!factor) {
        throw new Error(`Unknown factor keyword '${keyword}'`);
      }

      output += value * factor[0];
    } else {
      output += value;
    }
  }

  return output;
}
