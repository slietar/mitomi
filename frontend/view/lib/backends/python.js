import EventEmitter from './common/event-emitter.js';
import Plan from './common/plan.js';


export default class PythonBackend {
  constructor() {
    this._callbacks = {};
    this._nextCallbackId = 0;

    window._callbacks = this._callbacks;


    this.connection = null;
    this._protocol = null;

    this._chips = null;
    this._devices = null;
  }

  _registerCallback(callback, options = {}) {
    let id = this._nextCallbackId++;

    this._callbacks[id] = (...args) => {
      if (options.once) {
        this._unregisterCallback(id);
      }

      callback(...args);
    };

    return id;
  }

  _unregisterCallback(id) {
    delete this._callbacks[id];
  }


  async initialize() {
    if (!self.pywebview) {
      await new Promise((resolve) => {
        window.addEventListener('pywebviewready', () => {
          resolve();
        }, { once: true });
      });
    }
  }

  async reloadView() {
    return await pywebview.api.reload_view();
  }


  async listChips(options = {}) {
    this._chips = await pywebview.api.list_chips(options.reload || false);
    return this._chips;
  }

  async listDevices(options = {}) {
    this._devices = await pywebview.api.list_devices(options.reload || false);
    return this._devices;
  }

  async revealChipsDirectory() {
    await pywebview.api.reveal_chips_directory();
  }


  async connect(chip, device) {
    if (this.connection) {
      throw new Error('Cannot open a second connection');
    }

    let connection = new Connection(this);

    await connection.open(chip, device);

    this.connection = connection;
    this.connection.on('close', () => {
      this.connection = null;
    });

    return this.connection;
  }

  async disconnect() {
    await this.connection.close();
    this.connection = null;
  }


  static isSupported() {
    return true; // navigator.userAgent === 'Webview';
  }
}


class Connection extends EventEmitter {
  constructor(backend) {
    super();

    this._backend = backend;
    this._plan = null;

    this.chip = null;
    this.device = null;
  }

  get _protocol() {
    return this._plan && this._plan.protocol;
  }

  async open(chip, device) {
    await pywebview.api.connect(chip.id, device.id, {
      onlost: this._backend._registerCallback(() => {
        this.emit('lost');
        this.emit('close');
      })
    });

    this.chip = chip;
    this.device = device;
  }

  async close() {
    this.emit('close');
    await pywebview.api.disconnect();
  }


  async readAll() {
    return await pywebview.api.ctrl_read_all();
  }

  async writeAll(states) {
    return await pywebview.api.ctrl_write_all(states);
  }


  async promptProtocol() {
    let plan = await pywebview.api.protocol_prompt();

    if (plan) {
      this._plan = new Plan(plan);
      return this._plan;
    }

    return null;
  }

  async reloadProtocol() {
    this._plan = new Plan(await pywebview.api.protocol_reload());
    return this._plan;
  }

  async restoreProtocol() {
    let plan = await pywebview.api.protocol_restore();

    if (plan) {
      this._plan = new Plan(plan);
      return this._plan;
    }

    return null;
  }

  async updateProtocol(position) {
    this._plan = new Plan(await pywebview.api.protocol_update(position));
    return this._plan;
  }

  async startProtocol(options, progressCallback) {
    let run = {
      status: null,

      async pause() {
        await pywebview.api.protocol_pause();
      },
      async resume() {
        await pywebview.api.protocol_resume();
      },

      async skipState(position = null) {
        await pywebview.api.protocol_skip_state(position);
      }
    };

    let callbackId = this._backend._registerCallback((msg) => {
      switch (msg.type) {
        case 'update': {
          let state = this._protocol.states[msg.stateIndex];
          let stage = this._protocol.stages[state.indices[0]];
          let step = stage.steps[state.indices[1]];

          let resumeTime = new Date(msg.resumeTime * 1000);
          let startTime = new Date(msg.startTime * 1000);

          run.status = {
            stage,
            step,
            state,
            stateIndex: msg.stateIndex,

            history: msg.history,
            progress: msg.progress,
            resumeTime,
            startTime,

            paused: msg.paused,
            waiting: msg.waiting
          };

          progressCallback(run.status);

          break;
        }
        case 'end':
          this._backend._unregisterCallback(callbackId);
          run.status = null;
          progressCallback(null);
      }
    });

    await pywebview.api.protocol_start(options, callbackId);

    return run;
  }
}


function defer() {
  let resolve, reject;
  let promise = new Promise((_resolve, _reject) => {
    resolve = _resolve;
    reject = _reject;
  });

  return { promise, resolve, reject };
}
