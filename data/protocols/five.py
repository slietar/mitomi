from library import *


def protocol(chip):
  warnings = list()

  return Protocol(
    name="Protocol V",
    stages=[
      Stage(name="Stage 1", steps=[
        Step(name="Step 1", action=State(duration=parse_duration("10sec"), valves=chip.resolve("C3.C9", warnings=warnings))),
        Step(name="Step 2", action=Actions(name="Block {func()}", actions=[
          State(confirm="Please confirm")
        ])),
        Step(name="Step 3", action=State(duration=parse_duration("10sec"), valves=chip.resolve("C1.C2, MITOMI", warnings=warnings))),
      ])
    ],
    warnings=warnings
  )
