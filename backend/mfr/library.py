from collections import namedtuple
import re
import uuid


Stage = namedtuple("Stage", ["name", "steps"], defaults=[None, list()])
Step = namedtuple("Step", ["action", "description", "name"], defaults=[None, None])
State = namedtuple("State", ["confirm", "duration", "valves", "visible"], defaults=[None, None, set(), set()])
Actions = namedtuple("Actions", ["actions", "name"], defaults=[list(), None])
LinearizedState = namedtuple("LinearizedState", ["confirm", "duration", "indices", "valves", "visible"])


class Protocol:
  def __init__(self, *, id=None, name="Protocol", stages=list(), warnings=list()):
    self.id = id or str(uuid.uuid4())
    self.name = name
    self.stages = stages
    self.warnings = warnings

    self.states = self.linearize()

  def export(self):
    def export_linearized_state(state):
      return {
        'confirm': state.confirm,
        'duration': state.duration,
        'indices': state.indices,
        'valves': list(state.valves),
        'visible': list(state.visible)
      }

    def export_resolved_action(action, state_index):
      if isinstance(action, Actions):
        actions = list()
        states_count = 0

        for child in action.actions:
          action_inc, states_count_inc = export_resolved_action(child, state_index + states_count)
          actions.append(action_inc)
          states_count += states_count_inc

        return ({
          'type': "actions",
          'actions': actions,
          'name': action.name
        }, states_count)
      elif isinstance(action, State):
        return ({
          'type': "state",
          'stateIndex': state_index
        }, 1)

    stages = list()
    states = list()

    for stage_index, stage in enumerate(self.stages):
      exported_stage = {
        'name': stage.name,
        'stateIndex': len(states),
        'steps': list()
      }

      stages.append(exported_stage)

      for step_index, step in enumerate(stage.steps):
        state_index = len(states)

        exported_stage['steps'].append({
          'name': step.name,
          'action': export_resolved_action(step.action, state_index)[0],
          'stateIndex': state_index
        })

        states += self.linearize_action(step.action, [stage_index, step_index])

    return {
      'name': self.name,
      'stages': stages,
      'states': [export_linearized_state(state) for state in states],
      'warnings': self.warnings
    }


  def linearize(self):
    actions = list()

    for stage_index, stage in enumerate(self.stages):
      for step_index, step in enumerate(stage.steps):
        actions += self.linearize_action(step.action, [stage_index, step_index])

    return actions

  def linearize_action(self, action, indices = list()):
    if isinstance(action, Actions):
      actions = list()

      for child_index, child in enumerate(action.actions):
        actions += self.linearize_action(child, [*indices, child_index])

      return actions

    elif isinstance(action, State):
      return [LinearizedState(confirm=action.confirm, duration=action.duration, indices=indices, valves=action.valves, visible=action.visible)]


# ---


time_factors = {
  1: ["s", "sec", "second", "seconds"],
  60: ["m", "min", "minute", "minutes"],
  3600: ["h", "hr", "hrs", "hour", "hours"]
}.items()

time_regexp = re.compile(r"^ *(\d+(?:\.\d*)?|\d*\.\d+) *([a-z]+) *")
number_regexp = re.compile(r"^(?:\d+(?:\.\d*)?|\d*\.\d+)$")

# [Frozen]
# Returns a number that corresponds to the duration defined by 'value'.
# In particular, returns:
#   -  0.0 if 'value' is an empty string without whitespace,
#   - 'value' as a float if 'value' is a number,
#   -  None if 'value' does not match the syntax of a duration.
# Accepts leading and trailing whitespace.
def parse_duration(value):
  if isinstance(value, int) or isinstance(value, float) or number_regexp.match(value):
    return float(value)

  output = 0.0
  index = 0

  while index < len(value):
    match = time_regexp.match(value[index:])

    if not match:
      return None

    index += match.span()[1]
    subvalue, keyword = match.groups()

    factor = next((factor for factor, keywords in time_factors if keyword in keywords), None)

    if not factor:
      raise Exception(f"Unknown factor keyword '{keyword}'")

    output += float(subvalue) * factor

  return output


if __name__ == "__main__":
  for x in [*"""3 hours 2 minutes 5 seconds
3 hrs 2 min 5sec
3hr 2min5sec
3h2m5s
5s3h 2m
3h 2.0833m
3h 2m .0833m

 """.split("\n"), 10925]:
    print(f""""{x}" -> {parse_duration(x)}""")
