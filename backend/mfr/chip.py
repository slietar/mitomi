import pathlib
import re
import uuid
import yaml


ref_valve_regexp = re.compile(r"^([a-zA-Z]\w*)(?:(?:\.([a-zA-Z]\w*))|(\*))?$", re.ASCII)


class Chip:
  def __init__(self, *, builtin = False, id = None, name, path = None, schematic = None, valves = list()):
    self.builtin = builtin
    self.id = id if id else str(uuid.uuid4())
    self.name = name
    self.path = path
    self.schematic = schematic
    self.valves = valves

  def export(self):
    return {
      'builtin': self.builtin,
      'id': self.id,
      'name': self.name,
      'schematic': self.schematic,
      'valves': self.valves
    }

  # [Frozen]
  # Returns a set of indices of all valves that match 'value'.
  # Argument 'value' cannot contain comma-separated valve names, but can use the range and wildcard syntaxes.
  # Returns an empty set if 'value' is nullish, i.e. here an empty string or None.
  def parse_valve(self, value, *, warnings):
    if not value:
      return set()

    match = ref_valve_regexp.match(value)

    if not match:
      return None

    name, range_end, wildcard = match.groups()

    valves = set()
    valves_names = set()

    for index, target in enumerate(self.valves):
      target_name = target['name']

      if (
        wildcard is None
        and target_name == name
      ) or (
        range_end is not None
        and target_name >= name
        and target_name <= range_end
      ) or (
        wildcard is not None
        and target_name.startswith(name)
        and len(target_name) > len(name)
      ):
        valves.add(index)
        valves_names.add(target_name)

    if not valves:
      warnings.append(f"Missing valve '{value}'")
    elif range_end is not None:
      if min(valves_names) != name: warnings.append(f"Range lower bound should be '{min(valves_names)}', not '{name}'")
      if max(valves_names) != range_end: warnings.append(f"Range upper bound should be '{max(valves_names)}', not '{range_end}'")

    return valves

  # [Frozen input and output]
  # Returns a set of indices of all valves that match 'value'.
  # Argument 'value' can contain comma-separated valve names.
  # Accepts whitespace.
  def resolve(self, value, *, warnings = list()):
    output = set()

    if value:
      for item in (value.split(",") if isinstance(value, str) else value):
        stripped = item.strip()

        result = self.parse_valve(stripped, warnings=warnings)

        if result is not None:
          output |= result
        else:
          raise Exception(f"Invalid pattern '{stripped}'")

    return output


  def load(path, *, builtin = False, silent = False):
    path = pathlib.Path(path)

    try:
      data = yaml.safe_load(path.open())
    except Exception as e:
      if silent:
        return None
      else:
        raise e

    schematic = None

    if 'schematic' in data:
      schematic_path = path.parent / data['schematic']

      try:
        schematic = schematic_path.open().read()
      except:
        pass

    return Chip(
      builtin=builtin,
      id=data.get('id') or str(hash(path)),
      name=data['name'],
      path=path,
      schematic=schematic,
      valves=data['valves']
    )
