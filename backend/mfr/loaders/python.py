import importlib.util
import sys

from .. import library


sys.modules["library"] = library


class ProtocolPythonLoader:
  filetypes = "*.py"

  def __init__(self, path, *, chip):
    self.chip = chip
    self.protocol = None

    self.path = path
    self._load()

  def _load(self):
    spec = importlib.util.spec_from_file_location(self.path.stem, str(self.path))
    module = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(module)

    self.protocol = module.protocol(self.chip)

  def reload(self):
    self._load()

  def load(path, *, chip):
    if path.suffix != ".py":
      return None

    return ProtocolPythonLoader(path, chip=chip)


if __name__ == "__main__":
  import pathlib
  from ..chip import Chip

  chip = Chip.load("../../data/chips/mitomi1024.yml")
  loader = ProtocolPythonLoader.load(pathlib.Path("../../data/protocols/five.py"), chip=chip)

  from pprint import pprint
  # pprint(loader.protocol.export())
  pprint(loader.protocol.linearize())
