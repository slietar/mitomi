from collections import namedtuple
import re
import yaml

from .. import library


call_regexp = re.compile(r"^([a-zA-Z]\w*) *(?:\(([\w ,\.\$[\]]*)\))?$", re.ASCII)
ref_param_regexp = re.compile(r"^\$(\d+)$")


Func = namedtuple("Func", ["actions", "name", "parameters"])
RefDuration = namedtuple("RefDuration", ["param"])
RefValves = namedtuple("RefValves", ["aliases", "params", "valves"], defaults=[None, None, None])
Stage = namedtuple("Stage", ["name", "steps"])
Step = namedtuple("Step", ["action", "name"])

# Actions
Call = namedtuple("Call", ["arguments", "callee", "defaults", "restore"])
State = namedtuple("State", ["confirm", "defaults", "duration", "restore", "valves"])


class ProtocolYamlLoader:
  filetypes = "*.yml;*.yaml"

  def __init__(self, path, *, chip):
    self.chip = chip
    self.path = path
    self._load()

  def _load(self):
    data = yaml.safe_load(self.path.open())

    self.chips = data.get('chips')
    self.id = data.get('id')
    self.name = data['name']
    self.stages = list()

    self.warnings = list()


    self.aliases = dict()

    for alias, ref in data.get('aliases', dict()).items():
      self.aliases[alias] = self.parse_ref(ref)

    self.defaults = self.parse_ref(data.get('defaults', str()))


    self.functions = dict()

    for name, data_func in data.get('functions', dict()).items():
      parameters = list()
      actions = [self.parse_action(data_action, parameters) for data_action in data_func.get('actions', list())]

      for index, param in enumerate(parameters):
        if param is None:
          self.warnings.append(f"Unused parameter '${index}' in function '{name}'")

      self.functions[name] = Func(actions=actions, name=data_func.get('name'), parameters=parameters)


    for data_stage_index, data_stage in enumerate(data['stages']):
      stage = Stage(
        name=data_stage.get('name', f"Stage #{data_stage_index + 1}"),
        steps=list()
      )

      for data_step_index, data_step in enumerate(data_stage['steps']):
        step = Step(
          action=self.parse_action(data_step),
          name=data_step.get('name', f"Step #{data_step_index + 1}")
        )

        stage.steps.append(step)

      self.stages.append(stage)


    self.stack = list([self.resolve_aliases(self.defaults)])

    self.protocol = library.Protocol(
      id=self.id,
      name=self.name,
      stages=[
        library.Stage(
          name=stage.name,
          steps=[
            library.Step(
              action=self.resolve_action(step.action),
              name=step.name
            ) for step in stage.steps
          ]
        ) for stage in self.stages
      ],
      warnings=self.warnings
    )

  def reload(self):
    self._load()


  def parse_action(self, data_action, context = None):
    defaults = self.parse_defaults(data_action['defaults'] or str(), context=context) if 'defaults' in data_action else None
    restore = 'restore' in data_action

    if 'call' in data_action:
      arguments, callee = self.parse_call(data_action['call'], context)

      return Call(
        arguments=arguments,
        callee=callee,
        defaults=defaults,
        restore=restore
      )
    else:
      confirm = data_action.get('confirm')
      duration = self.parse_value(str(data_action['duration']), context=context, value_type='duration') if (not confirm) and ('duration' in data_action) else None
      valves = self.parse_ref(data_action.get('valves') or str(), context)

      return State(
        confirm=confirm,
        defaults=defaults,
        duration=duration,
        restore=restore,
        valves=valves
      )

  def parse_call(self, input_value, context):
    match = call_regexp.match(input_value)

    if not match:
      raise Exception(f"Invalid call expression '{input_value}'")

    callee, args = match.groups()

    if args is None:
      args = str()

    arguments = list()
    function = self.functions.get(callee)

    if not function:
      raise Exception(f"Call to missing function '{callee}'")


    def get_type(value):
      if isinstance(value, float) or isinstance(value, RefDuration):
        return 'duration'
      if isinstance(value, RefValves):
        return 'valve'

      return None


    args_stripped = args.strip()
    args_insulated = list()

    cable = list()
    insulated = None

    if args_stripped:
      for comp in args_stripped.split(","):
        stripped = comp.strip()

        if insulated:
          if stripped.endswith("]"):
            insulated.append(stripped[:-1])
            values = list()

            for comp in insulated:
              values.append(self.parse_value(comp, context=context, value_type='valve'))

            args_insulated.append({ 'type': 'valve', 'value': [merge_valves(*values)] })
            insulated = None
          else:
            insulated.append(stripped)

          continue

        elif stripped.startswith("["):
          if len(cable) > 0:
            args_insulated.append({ 'type': 'valve', 'value': cable })
            cable = list()

          if stripped.endswith("]"):
            args_insulated.append({ 'type': 'valve', 'value': [self.parse_value(stripped[1:-1], context=context, value_type='valve')] })
          else:
            insulated = [stripped[1:]]

          continue

        value = self.parse_value(stripped, context=context)
        value_type = get_type(value)

        if value_type == 'valve':
          cable.append(value)
        else:
          if len(cable) > 0:
            args_insulated.append({ 'type': 'valve', 'value': cable })
            cable = list()

          args_insulated.append({ 'type': value_type, 'value': [value] })

    if insulated:
      raise Exception("Unfinished insulator")

    if len(cable) > 0:
      args_insulated.append({ 'type': 'valve', 'value': cable })


    item_arg_index = 0
    item_subarg_index = 0

    for param_index, param_type in enumerate(function.parameters):
      if param_type is None:
        arguments.append(None)
        continue

      if item_arg_index >= len(args_insulated):
        if param_type == 'valve':
          arguments.append(RefValves())
          continue
        else:
          raise Exception(f"Missing argument ${param_index} when calling 'callee()', expected value of type '{param_type}'")

      item_arg = args_insulated[item_arg_index]
      arg = item_arg['value']

      if item_arg['type'] != param_type:
        raise Exception(f"Invalid type of argument ${param_index} when calling '{callee}()', expected '{param_type}', found '{item_arg['type']}'")

      if param_type == 'valve':
        next_param_type = next((param for param in function.parameters[(param_index + 1):] if param is not None), None)

        if item_subarg_index == 0 and ((next_param_type is None) or (next_param_type != 'valve')):
          arguments.append(merge_valves(*arg))
          item_arg_index += 1
        else:
          arguments.append(arg[item_subarg_index])

          if item_subarg_index + 1 < len(arg):
            item_subarg_index += 1
          else:
            item_arg_index += 1
            item_subarg_index = 0

      else:
        arguments.append(arg[0])
        item_arg_index += 1

    if item_arg_index < len(args_insulated):
      raise Exception(f"Supernumerary argument ${len(arguments)} when calling '{callee}()'")

    return arguments, callee


  def parse_defaults(self, value, context = None):
    stripped = value.strip()
    additive = stripped.startswith("+")

    return {
      'additive': additive,
      'ref': self.parse_ref(stripped[(1 if additive else 0):], context=context)
    }


  # Returns a RefValves with valves that match 'value'.
  # Argument 'value' can contain multiple items, aliases and parameters.
  def parse_ref(self, value, context = None):
    items = value.split(",")
    refs = list()

    for item in items:
      refs.append(self.parse_value(item, context=context, value_type='valve'))

    return merge_valves(*refs)


  # Returns a set of indices that match 'value'.
  # Argument 'value' is a single item corresponding to either an alias, a valve name or a duration.
  # Accepts whitespace.
  def parse_value(self, value, context = None, value_type = None):
    match = ref_param_regexp.match(value.strip())

    if match:
      param, = match.groups()
      param = int(param)
      param_type = self.use_param(param, context, type=value_type)

      return RefValves(params=set([param])) if param_type == 'valve' else RefDuration(param)

    if value_type is None or value_type == 'duration':
      duration = library.parse_duration(value)

      if duration is not None:
        return duration

    if value_type is None or value_type == 'valve':
      stripped = value.strip()

      if stripped in self.aliases:
        return RefValves(aliases=set([stripped]))
      else:
        valves = self.chip.parse_valve(stripped, warnings=self.warnings)

        if valves is not None:
          return RefValves(valves=valves)

    raise Exception(f"Invalid value '{value}'")


  # type = None: unknown type
  def use_param(self, param, context, *, type = None):
    if context is None:
      raise Exception(f"Reference to parameter '${param}' without context")

    while len(context) < param + 1:
      context.append(None)

    if context[param] is None:
      if type is not None:
        context[param] = type
      else:
        raise Exception("Unknown type of parameter '${param}'")
    elif (type is not None) and (context[param] != type):
      raise Exception(f"Invalid type of parameter '${param}', expected '{type}', found '{context[param]}'")

    return context[param]


  def resolve_aliases(self, ref):
    valves = set(ref.valves or set())

    for alias in ref.aliases:
      valves |= self.resolve_aliases(self.aliases[alias])

    return valves

  # Stack starts at 'stack_start'.
  def resolve_action(self, action, params = None, stack_start = 1):
    def resolve_value(value, value_type):
      if value_type == 'duration':
        if isinstance(value, RefDuration): return params[value.param]
        if isinstance(value, float): return value
      elif value_type == 'valve':
        aliases = set(value.aliases or set())
        valves = set(value.valves or set())

        for param_index in (value.params or set()):
          aliases |= params[param_index].aliases or set()
          valves |= params[param_index].valves or set()

        return RefValves(aliases=aliases, valves=valves)

    out_action = None

    if action.defaults:
      ref = self.resolve_aliases(resolve_value(action.defaults['ref'], 'valve'))
      self.stack.append((ref ^ self.stack[-1]) if action.defaults['additive'] else ref)

    if isinstance(action, Call):
      callee = self.functions[action.callee]
      args = [resolve_value(action.arguments[param_index], param_type) for param_index, param_type in enumerate(callee.parameters)]
      call_stack_start = len(self.stack)
      actions=[self.resolve_action(sub_action, args, stack_start=call_stack_start) for sub_action in callee.actions]
      self.stack = self.stack[0:call_stack_start]

      out_action = library.Actions(actions=actions, name=callee.name or f"Call to {{{action.callee}()}}")
    elif isinstance(action, State):
      duration = resolve_value(action.duration, 'duration') if action.duration is not None else None
      visible = self.resolve_aliases(resolve_value(action.valves, 'valve'))

      valves = visible ^ self.stack[-1]

      out_action = library.State(confirm=action.confirm, duration=duration, valves=valves, visible=visible)

    if action.restore:
      if len(self.stack) <= stack_start:
        raise Exception(f"Cannot restore defaults lower than context baseline (index {stack_start})")

      self.stack.pop()

    return out_action


  def load(path, *, chip):
    if path.suffix != ".yml" and path.suffix != ".yaml":
      return None

    return ProtocolYamlLoader(path, chip=chip)


def merge_valves(*args):
  aliases = set()
  params = set()
  valves = set()

  for arg in args:
    if arg.aliases: aliases |= arg.aliases
    if arg.params: params |= arg.params
    if arg.valves: valves |= arg.valves

  return RefValves(aliases=aliases, params=params, valves=valves)


# ---


if __name__ == "__main__":

  from ..chip import Chip
  from pathlib import Path

  chip = Chip.load("../../data/chips/mitomi1024.yml")

  # loader = ProtocolYamlLoader.load(Path("../../data/protocols/MITOMI_general.yml"), chip=chip)
  loader = ProtocolYamlLoader.load(Path("../../data/protocols/six.yml"), chip=chip)
  # loader = ProtocolYamlLoader.load(Path("../../data/protocols/multiplexing.yml"), chip=chip)

  from pprint import pprint
  import json

  # print("\nOutput")
  # pprint(loader.functions)
  # pprint(loader.protocol.stages[0].steps[0])
  # pprint(loader.protocol.export()['states'])

  # pprint(protocol.export())
  # print(json.dumps(loader.protocol.export()))
  # x = protocol.resolve_action(protocol.stages[0].steps[2].action)
  # pprint(x)
  # print(protocol.linearize_action(x))
  # print(protocol.resolve_linearize())

  """
  print("\nAliases -->")
  pprint(loader.aliases)

  print("\nStep 0 -->")
  pprint(loader.stages[0].steps[0])

  print("\nDefaults -->")
  pprint(protocol.defaults)

  print("\nStep 1 -->")
  pprint(loader.stages[0].steps[1])

  print("\nFunctions -->")
  pprint(loader.functions)

  print("\nWarnings -->")
  pprint(loader.warnings)
  """


  # print(parse_ref("MITOMI.SANDWICH", chip['valves']))


  if True:
    from ..controllers.dummy import DummyController
    from ..protocol_run import ProtocolRun, ProtocolRunPlan

    def callback():
      pprint(ProtocolRun.export_history(run.history))

    controller = DummyController()
    controller.connect(DummyController.list()[0]['id'])

    plan = ProtocolRunPlan(loader, position={
      'progress': 0,
      'state_index': 0
    })

    run = ProtocolRun(plan, callback=callback, controller=controller, log_path="a.log")
    run.start()

    import time
    time.sleep(0.5)
    run.pause()
    time.sleep(0.5)
    run.resume()
