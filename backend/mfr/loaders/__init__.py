from pathlib import Path

from .python import ProtocolPythonLoader
from .yaml import ProtocolYamlLoader


loaders = [ProtocolPythonLoader, ProtocolYamlLoader]
file_types = (f"""Protocol ({";".join([Loader.filetypes for Loader in loaders])})""",)

def load(path, *, chip):
  for Loader in loaders:
    loader = Loader.load(Path(path), chip=chip)

    if loader:
      return loader

  return None
