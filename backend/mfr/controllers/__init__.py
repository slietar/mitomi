class Controller:
  def chip_write_all(self, chip, state):
    value = 0

    for index, valve in enumerate(chip.valves):
      if (state & (1 << index)) > 0:
        if 'channel' in valve:
          value |= (1 << valve['channel'])

    self.relay_write_all(value)

  def chip_read_all(self, chip):
    value = self.relay_read_all()
    state = 0

    for index, valve in enumerate(chip.valves):
      if 'channel' in valve and (value & (1 << valve['channel'])) > 0:
        state |= (1 << index)

    return state
