import serial, serial.tools.list_ports

from . import Controller


class NumatoRelay32Controller(Controller):
  def __init__(self, *, onlost = None):
    self._onlost = onlost
    self._serial = None


  def connect(self, path):
    if self._serial:
      raise Exception("Already connected")

    self._serial = serial.Serial(path, 9600, timeout=0.5)

    if self.get_version() != 8:
      raise Exception("Unknown version")

  def disconnect(self):
    if not self._serial:
      raise Exception("Not connected")

    print("Closing")
    self._serial.close()
    self._serial = None


  def get_version(self):
    return int(self._request("ver"))

  def relay_write(self, index, value):
    self._order("relay " + ("on" if value else "off") + " " + get_relay_number(index))

  def relay_read_all(self):
    return int(self._request("relay readall"), 16)

  def relay_write_all(self, value):
    self._order("relay writeall " + format(value, '08x'))


  def _order(self, text):
    if not self._serial:
      raise Exception("Not connected")

    try:
      self._serial.write((text + "\r").encode("utf-8"))
    except serial.serialutil.SerialException as e:
      if self._onlost:
        self._onlost()

      raise e

    self._serial.readline()

  def _request(self, text):
    self._order(text)

    result = self._serial.readline().decode("utf-8").strip()

    if not result or result == ">":
      raise Exception("No response")

    return result


  def list():
    return [{
      'id': device.device,
      'name': device.name,
      'description': device.description if device.description != 'n/a' else None,
      'supported': (device.pid == 0x0c04 and device.vid == 0x2a19)
    } for device in serial.tools.list_ports.comports()]


def get_relay_number(index):
  return str(index) if index < 10 else chr(0x41 + (index - 10))


if __name__ == "__main__":
  # from chip import Chip
  from pathlib import Path

  c = NumatoRelay32Controller()
  # chip = Chip.load(Path("../../data/chips/mitomi1024.yml"))

  # c.connect("/dev/cu.usbmodem1101")

  c.relay_write_all(1 << 31)
  print(bin(c.relay_read_all()))
  # c.chip_write_all(chip, 0b10000)
  # print(bin(c.chip_read_all(chip)))
