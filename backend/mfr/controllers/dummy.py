import time

from . import Controller


class DummyController(Controller):
  def __init__(self, *, onlost = None):
    self._state = None

  def connect(self, device_id):
    if device_id == "dummy.device3":
      time.sleep(2)
      raise Exception("Device broken")

    self._state = (1 << 2) | (1 << 3) | (1 << 10)

  def disconnect(self):
    self._state = None

  def relay_read(self, channel):
    mask = 1 << channel
    return (self._state & mask) != 0

  def relay_write(self, channel, value):
    mask = 1 << channel
    self._state = (self._state & ~mask) | (mask if value else 0)

  def relay_read_all(self):
    return self._state

  def relay_write_all(self, state):
    self._state = state & 0xffffffff


  def list():
    return [
      { 'id': "dummy.device", 'name': "Dummy device", 'description': None, 'supported': True },
      { 'id': "dummy.device2", 'name': "Dummy device (unsupported)", 'description': None, 'supported': False },
      { 'id': "dummy.device3", 'name': "Dummy device (broken)", 'description': None, 'supported': False }
    ]
