import datetime
import json
from pathlib import Path
import threading


class ProtocolRun:
  def __init__(self, plan, *, callback = None, controller, log_path):
    self._chip = plan.loader.chip
    self._plan = plan

    self.history = plan.history
    self.states = plan.loader.protocol.states

    self.progress = 0 # plan.position['progress']
    self.state_index = plan.position['state_index'] # None = run finished

    self.resume_time = None
    self.start_time = None
    self.paused = False
    self.waiting = False

    self._callback = callback
    self._controller = controller
    self._entry = None
    self._timer = None

    self._log = None
    self._log_path = Path(log_path)


  def start(self):
    self._log = self._log_path.open("w", buffering=1) # 1 = line buffered
    now = get_time().isoformat()

    self._log.write(f"# Protocol runner 0.1.0\n")
    self._log.write(f"{now} [CHIP] {json.dumps({ 'info': self._plan.chip_info, 'path': str(self._chip.path) })}\n")
    self._log.write(f"{now} [PROTOCOL] {json.dumps(str(self._plan.loader.path))}\n")

    if len(self.history) > 0:
      paused = False

      for entry in self.history:
        self._log_state(entry['state_index'], entry['events'][0])

        for event in entry['events'][1:]:
          time = event['time'].isoformat()

          if not paused and event['paused']:
            self._log.write(f"{time} [PAUSE] {event['progress']}\n")
          elif paused and not event['paused']:
            self._log.write(f"{time} [RESUME]\n")

          paused = event['paused']

      self._log.write("# Restoring run\n")
      self._entry = self.history[-1]

    self._start_state(progress=self._plan.position['progress'])

  def stop(self):
    self._stop_timer()
    self._controller.relay_write_all(0)

    self.state_index = None
    self._update()

    # Log status
    self._log.write(f"{get_time().isoformat()} [DONE]\n")
    self._log.close()


  def _start_state(self, *, progress = 0):
    state = self.states[self.state_index]

    # Write (and read) to the controller
    valves_state_req = 0

    for valve_index in state.valves:
      valves_state_req |= (1 << valve_index)

    self._controller.chip_write_all(self._chip, valves_state_req)
    valves_state = self._controller.chip_read_all(self._chip)

    # Save status
    self.progress = progress
    self.waiting = state.duration is None

    time = get_time()
    self.start_time = time # deprecated
    self.resume_time = time

    # Append to history and log
    if not self._entry or self.state_index != self._entry['state_index']:
      self._entry = {
        'events': list(),
        'state_index': self.state_index
      }

      self.history.append(self._entry)

    self._entry['events'].append({
      'paused': self.paused,
      'progress': self.progress,
      'time': time,
      'valves_state': valves_state
    })

    self._log_state()

    # Notify the frontend
    self._update()

    # Start the timer
    if not (self.paused or self.waiting):
      def callback():
        self._timer = None
        self._end_state()

      self._timer = threading.Timer(state.duration * (1 - progress), callback)
      self._timer.start()


  def _end_state(self, position = None):
    """
    time = get_time()
    state = self.states[self.state_index]

    self._entry['events'].append({
      **self._entry['events'][-1],
      'progress': self.progress + (0 if self.paused or (state.duration is None) else (time.timestamp() - self.resume_time.timestamp()) / state.duration),
      'time': time
    })

    self._log_state()
    """

    self.state_index = position['state_index'] if position else (self.state_index + 1)

    if self.state_index < len(self.states):
      self._start_state(progress=(position['progress'] if position else 0))
    else:
      self.stop()

  def _update(self):
    if self._callback:
      self._callback()

  def _stop_timer(self):
    if self._timer:
      self._timer.cancel()
      self._timer = None

  def _log_state(self, arg_state_index = None, arg_event = None):
    event = arg_event or self._entry['events'][-1]
    state_index = arg_state_index if arg_state_index is not None else self._entry['state_index']

    self._log.write(f"{event['time'].isoformat()} [STATE] {state_index} {event['progress']} {event['valves_state']}\n")


  def pause(self):
    if self.paused or self.waiting:
      return

    self._stop_timer()

    time = get_time()
    state = self.states[self.state_index]

    self.paused = True
    self.progress += (time.timestamp() - self.resume_time.timestamp()) / state.duration

    self._entry['events'].append({
      **self._entry['events'][-1],
      'paused': True,
      'progress': self.progress,
      'time': time
    })

    self._log.write(f"{time.isoformat()} [PAUSE] {self.progress}\n")
    self._update()

  def resume(self):
    if not self.paused or self.waiting:
      return

    state = self.states[self.state_index]

    self.paused = False
    self.resume_time = get_time()

    self._entry['events'].append({
      **self._entry['events'][-1],
      'paused': False,
      'time': self.resume_time
    })

    def callback():
      self._timer = None
      self._end_state()

    self._timer = threading.Timer((1 - self.progress) * state.duration, callback)
    self._timer.start()

    self._log.write(f"{self.resume_time.isoformat()} [RESUME]\n")
    self._update()

  def skip_state(self, position = None):
    self._stop_timer()
    self._end_state(position)


  def position(self):
    return {
      'progress': self.progress,
      'state_index': self.state_index
    }


  def export_history(history):
    return [{
      'events': [{
        'paused': event['paused'],
        'progress': event['progress'],
        'time': event['time'].timestamp() * 1000,
        # 'valvesState': s['valves_state']
      } for event in entry['events']],
      'stateIndex': entry['state_index']
    } for entry in history]


def get_time():
  return datetime.datetime.now()


class ProtocolRunPlan:
  # def __init__(self, loader, *, chip_info = None, status = { 'progress': 0, 'state_index': 0 }):
  def __init__(self, loader, *, chip_info = None, history=list(), position):
    self.chip_info = chip_info
    self.loader = loader
    self.history = history
    self.position = position

  def export(self):
    return {
      'chipInfo': self.chip_info,
      'protocol': self.loader.protocol.export(),
      'history': ProtocolRun.export_history(self.history),
      'position': {
        'progress': self.position['progress'],
        'stateIndex': self.position['state_index']
      } if self.position else None
    }
