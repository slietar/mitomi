from datetime import datetime
from pathlib import Path
import json
import re

from .chip import Chip
from . import loaders
from .protocol_run import ProtocolRunPlan


def load_log(path):
  file = Path(path).open()

  regexp_chip = re.compile(r"^[\dT\.\:-]+ \[CHIP] (.+)$")
  regexp_done = re.compile(r"^[\dT\.\:-]+ \[DONE]$")
  regexp_pause = re.compile(r"^([\dT\.\:-]+) \[PAUSE] (\d+(?:\.\d+)?)$")
  regexp_protocol = re.compile(r"^[\dT\.\:-]+ \[PROTOCOL] (.+)$")
  regexp_resume = re.compile(r"(^[\dT\.\:-]+) \[RESUME]$")
  regexp_state = re.compile(r"^([\dT\.\:-]+) \[STATE] (\d+) (\d+(?:\.\d+)?) (\d+)$")

  history = list()
  chip_data = None
  protocol_path = None

  entry = None
  paused = False
  done = False

  for line in file.read().split("\n"):
    if len(line) < 1 or line.startswith("#"):
      continue

    if not done:
      if not chip_data:
        match_chip = regexp_chip.match(line)

        if match_chip:
          groups = match_chip.groups()
          chip_data = json.loads(groups[0])

          continue

      if chip_data and not protocol_path:
        match_protocol = regexp_protocol.match(line)

        if match_protocol:
          groups = match_protocol.groups()
          protocol_path = Path(json.loads(groups[0]))

          continue


      if chip_data and protocol_path:
        match_pause = regexp_pause.match(line)

        if match_pause and entry and not paused:
          groups = match_pause.groups()
          paused = True

          entry['events'].append({
            'paused': paused,
            'progress': float(groups[1]),
            'time': datetime.fromisoformat(groups[0]),
            'valves_states': None
          })

          continue

        match_resume = regexp_resume.match(line)

        if match_resume and entry and paused:
          groups = match_resume.groups()
          paused = False

          entry['events'].append({
            **entry['events'][-1],
            'paused': paused,
            'time': datetime.fromisoformat(groups[0])
          })

          continue

        match_state = regexp_state.match(line)

        if match_state:
          groups = match_state.groups()
          state_index = int(groups[1])

          if not entry or state_index != entry['state_index']:
            entry = {
              'events': list(),
              'state_index': state_index
            }

            history.append(entry)

          entry['events'].append({
            'paused': paused,
            'progress': float(groups[2]),
            'time': datetime.fromisoformat(groups[0]),
            'valves_state': int(groups[3])
          })

          continue

        match_done = regexp_done.match(line)

        if match_done:
          done = True
          continue

    print(f"Warning: ignoring line '{line}'")


  chip = Chip.load(chip_data['path'])
  loader = loaders.load(protocol_path, chip=chip)

  position = None

  if not done:
    if len(history) > 0:
      last_entry = history[-1]
      last_event = last_entry['events'][-1]

      position = {
        'progress': last_event['progress'],
        'state_index': last_entry['state_index']
      }
    elif len(loader.protocol.states) > 0:
      position = {
        'progress': 0,
        'state_index': 0
      }

  return ProtocolRunPlan(loader, chip_info=chip_data['info'],
    history=history,
    position=position
  )


if __name__ == '__main__':
  # log = load_log("../../data/runs/2021-06-07T21:50:00.log")
  plan = load_log("../../data/runs/x.log")

  from pprint import pprint
  pprint(plan.position)
  # pprint(chip_info)
  # pprint(protocol_path)
  # pprint(output)
