from pathlib import Path

from view.backend import Backend

if __name__ == "__main__":
  backend = Backend(builtin_dir=Path(__file__).parent.absolute() / "builtin")
  backend.start()
