import appdirs
import json
import os
import pathlib
import subprocess
import sys
import time
import webview

from .mfr.chip import Chip
from .mfr.controllers.numato import NumatoRelay32Controller
from .mfr.controllers.dummy import DummyController
from .mfr.protocol_run import ProtocolRun, ProtocolRunPlan
from .mfr import loaders
from .mfr.log import load_log


def resource_path(relative_path):
  try:
    base_path = sys._MEIPASS
  except Exception:
    base_path = os.path.abspath(".")

  return os.path.join(base_path, relative_path)


debug = bool(os.environ.get("DEBUG"))

class Backend:
  def __init__(self, builtin_dir):
    self.builtin_dir = pathlib.Path(builtin_dir)
    self.builtin_chips_dir = self.builtin_dir / "chips"

    self.data_dir = pathlib.Path(appdirs.user_data_dir(appname="MITOMI", appauthor="LBNC"))
    self.data_chips_dir = self.data_dir / "chips"
    self.logs_dir = self.data_dir / "runs"

    self.data_dir.mkdir(parents=True, exist_ok=True)
    self.data_chips_dir.mkdir(exist_ok=True)
    self.logs_dir.mkdir(exist_ok=True)


    self.chips = None
    self.devices = None

    self.controllers = [NumatoRelay32Controller, DummyController]

    self.chip = None
    self.device = None
    self.plan = None
    self.protocol_run = None

    self.controller = None
    self.window = None

  def _call_callback(self, callback_id, *args):
    self.window.evaluate_js(f"window._callbacks[{callback_id}](...{json.dumps(args)})")


  def connect(self, chip_id, device_id, options = dict()):
    self.chip = next(chip for chip in self.chips if chip.id == chip_id)
    self.device = next(device for device in self.devices if device['id'] == device_id)

    Controller = self.controllers[self.device['controller_index']]

    def lost_callback():
      self._on_disconnect()

      if 'onlost' in options:
        self._call_callback(options['onlost'])

    controller = Controller(onlost=lost_callback)
    controller.connect(self.device['id'])

    # Only if the connection succeeds
    self.controller = controller

  def disconnect(self):
    if self.controller:
      self.controller.disconnect()

    self._on_disconnect()

  def _on_disconnect(self):
    self.controller = None
    # TODO: also handle run


  def list_chips(self, reload = False):
    if self.chips is None or reload:
      self.chips = list()

      for chip_path in self.builtin_chips_dir.glob("**/*.yml"):
        self.chips.append(Chip.load(chip_path, builtin=True))

      for chip_path in self.data_chips_dir.glob("**/*.yml"):
        chip = Chip.load(chip_path, silent=True)

        if chip:
          self.chips.append(chip)

    return [chip.export() for chip in self.chips]

  def list_devices(self, reload = False):
    if self.devices is None or reload:
      self.devices = [{ **device, 'controller_index': index } for index, Controller in enumerate(self.controllers) for device in Controller.list()]

    return self.devices

  def reveal_chips_directory(self):
    if os.name == "nt":
      os.startfile(self.data_chips_dir)
    else:
      subprocess.call(["open", "-R", str(self.data_chips_dir)])


  def ctrl_read_all(self):
    return self.controller.chip_read_all(self.chip)

  def ctrl_write_all(self, state):
    self.controller.chip_write_all(self.chip, state)


  def protocol_start(self, options, callback_id):
    def callback():
      run = self.protocol_run

      status = {
        'type': "update",
        'history': ProtocolRun.export_history(run.history),
        'paused': run.paused,
        'progress': run.progress,
        'resumeTime': run.resume_time.timestamp(),
        'startTime': run.start_time.timestamp(), # deprecated
        'stateIndex': run.state_index,
        'waiting': run.waiting
      } if run.state_index is not None else { 'type': "end" }

      self._call_callback(callback_id, status)

    if 'chipInfo' in options:
      self.plan.chip_info = options['chipInfo']

    log_filename = time.time() + ".log"

    self.protocol_run = ProtocolRun(self.plan, callback=callback, controller=self.controller, log_path=(self.logs_dir / log_filename))
    self.protocol_run.start()

  def protocol_stop(self):
    self.protocol_run.stop()
    self.protocol_run = None

    self.plan = None

  def protocol_prompt(self):
    if not self.chip:
      return None

    filepaths = self.window.create_file_dialog(dialog_type=webview.OPEN_DIALOG, directory="", file_types=loaders.file_types)
    # filepaths = ["/users/simon/projects/pdb/data/protocols/MITOMI_general.yml"]
    # filepaths = ["/users/simon/projects/pdb/data/protocols/six.yml"]

    if not filepaths:
      return None

    loader = loaders.load(filepaths[0], chip=self.chip)

    if not loader:
      return None

    plan = ProtocolRunPlan(loader, position={
      'progress': 0,
      'state_index': 0
    })

    self.plan = plan
    return plan.export()

  def protocol_reload(self):
    if self.protocol_run:
      self.protocol_run.pause()

    # Might raise an Exception.
    self.plan.loader.reload()

    if self.protocol_run:
      self.plan.history = self.protocol_run.history
      self.plan.position = self.protocol_run.position()

      self.protocol_run.stop()
      self.protocol_run = None

    return self.plan.export()

  def protocol_restore(self):
    if not self.chip:
      return None

    filepaths = self.window.create_file_dialog(dialog_type=webview.OPEN_DIALOG, directory=str(self.logs_dir), file_types=("Protocol run log files (*.log)",))

    if not filepaths:
      return None

    self.plan = load_log(filepaths[0])
    return self.plan.export()

  def protocol_pause(self):
    self.protocol_run.pause()

  def protocol_resume(self):
    self.protocol_run.resume()

  def protocol_skip_state(self, position):
    self.protocol_run.skip_state({
      'progress': position['progress'],
      'state_index': position['stateIndex']
    } if position else None)

  def protocol_update(self, position):
    self.plan.position = {
      'progress': position['progress'],
      'state_index': position['stateIndex']
    }

    return self.plan.export()


  def start(self):
    self.window = self.create_view()
    webview.start(debug=True, user_agent="Webview")

    # Executed after all windows are closed.
    if self.controller:
      self.controller.disconnect()
      self.controller = None

  def create_view(self):
    if debug:
      window = webview.create_window("Protocol runner", "http://localhost:8081/tmp", js_api=self, min_size=(800, 600), width=1200, height=800)
    else:
      html_file = open(resource_path("index.html"), "r", encoding="utf-8")
      window = webview.create_window("Protocol runner", html=html_file.read(), js_api=self, min_size=(800, 600), width=1200, height=800)

    return window

  def reload_view(self):
    old_window = self.window
    self.window = self.create_view()
    old_window.destroy()
