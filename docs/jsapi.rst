JavaScript API
==============


General classes
---------------

.. js:class :: Backend

  .. js:method :: promptDevice()

    :return: A promise that resolves with a :js:class:`Connection` instance if the request succeeded or ``null`` if the user canceled the prompt.

    Ask the user to allow access to a new device that otherwise cannot be obtained through :js:meth:`listDevices`. This method is optional in implementations.

    .. note :: This method is useful as an alternative to :js:meth:`connect()` when the script doesn't have a direct access to the list of devices as in a browser with the Web Serial API. The presence of this method will allow the user to explicitly add a device through a specific "Add a device" button.

  .. js:method :: listChips([options])

    :param Object options:

      - ``reload`` (Boolean) – Whether to reload the internal list of chips if it was already loaded.

    :return: A promise that resolves with a list of :js:class:`Chip` instances.

    Request the list of chips.

  .. js:method :: listDevices([options])

    :param Object options:

      - ``reload`` (Boolean) – Whether to reload the internal list of devices if it was already loaded.

    :return: A promise that resolves with a list of :js:class:`Device` instances.

    Request the list of devices.

  .. js:method :: revealChipsDirectory()

    :return: A promise resolved once the action is completed.

    Optional method in implementations.

  .. js:method :: connect(chip, device)

    :param Chip chip: The corresponding chip.
    :param Device device: The device to which to connect.
    :throws: An error when the device is incompatible, missing, already in use or does not respond for another reason.
    :return: A promise that resolves with a :js:class:`Connection` instance.

    Initiate connection with a device.

  .. py:staticmethod :: isSupported()

    :return: A promise that resolves with a boolean.

    Indicates whether this backend is supported on the current platform.


.. js:class :: Connection

  .. js:method :: close()

    :return: A promise that resolves once the connection is closed.

  .. js:method :: readAll()

    :return: A promise that resolves with the state of all valves as a 32-bit integer.

  .. js:method :: writeAll(state)

    :param Number state: The state of all valves as a 32-bit integer.
    :return: A promise that resolves once the operation is complete.

  .. js:method :: protocolPrompt()

  .. js:method :: protocolReload()

  .. js:method :: protocolRestore()

  .. js:method :: protocolStart(options, progressCallback)

    :param progressCallback: A function to be called regularly with the value of ``run.status``, or ``null`` when the run ends.

    :return: A promise that resolves with a :js:class:`Run` instance.

  .. js:method :: protocolUpdate(position)


Protocol running
----------------

.. js:class :: Run

  .. js:method :: pause()

    :throws: An error when the run is already paused (but not when it is waiting).

  .. js:method :: resume()

    :throws: An error when the run is already running.

  .. js:method :: skipState([position])

    :param Object position: The position of the state to which the run should continue. Optional.

  .. js:method :: stop()


.. js:class :: RunStatus

  .. js:attribute :: stage

    An object describing the stage of the current state.

  .. js:attribute :: step

    An object describing the step of the current state.

  .. js:attribute :: state

    An object describing the current state.

  .. js:attribute :: stateIndex

    The index of the state, unique in the protocol.

  .. js:attribute :: history

    An array of `history entries` objects with the following properties:

    - ``stateIndex`` – The index of the state to which the entry refers. Multiple entries can have the same index, which can happen if the user went back to a previous state.
    - ``events`` – An array of events that describe the state's lifecycle, with the following properties:

      - ``paused`` – Whether the run was paused from this event to the next.
      - ``progress`` – The run's progress when this event occured.
      - ``time`` – The timestamp when this event occured.
      - ``valves`` – A 32-bit number which represents the state of the valves when XORed to the normal valves defined for this state. It remains 0 when no manual control is involved. (To be added.)

    .. note :: There should always be at least one event defined, usually with ``progress`` set to 0.

    .. caution :: The information in this property should not be used to make assumptions on the state to run next. When starting the run, the first state may or may not have an existing history entry.

  .. js:attribute :: progress

    The run's progress since the last update (not continuously updated).

  .. js:attribute :: resumeTime

  .. js:attribute :: startTime

    :deprecated: Use :js:attr:`history` instead.

  .. js:attribute :: paused

  .. js:attribute :: waiting

    .. note :: Pausing and waiting are two independent flags for the run; they are not exclusive.
