Chip format
================


Example
-------

.. code :: yaml

  ---
  name: MITOMI 1024
  schematic: mitomi1024.svg
  valves:
    - name: mult1
      channel: 0
      group: 0
      schematic: [1, 17]
    - name: mult2
      channel: 1
      group: 0
      schematic: [1, 18]
    - name: mult3
      channel: 2
      group: 0
      schematic: [1, 15]
    - name: mult4
      channel: 3
      group: 0
      schematic: [1, 16]



Aliases
-------

.. warning :: Not implemented.

Aliases can be directly defined in a chip's definition. For example, one of the valves in a group of 8 inputs might be frequently used as waste, but one may want to avoid naming it accordingly to preserve the ``in1-8`` pattern that mirrors the channel layout on the chip. To solve this issue, aliases can be defined by adding comma-separated identifiers to a valve's name:

.. code :: yaml

  - name: in8, waste
    channel: 7

The first identifier remains the main one, and it is this one that will be displayed.


Schematics
----------

Schematic creation can be achieved using the ``create-schematic`` package on npm. This package will turn a DXF schematic into SVG, and will group drawn components that belong to the same channel.

.. code :: sh

  $ create-schematic --input schematic.dxf --output schematic.svg
  $ create-schematic < schematic.dxf > schematic.svg

Tolerance must be adjusted using the ``--tolerance`` option to define an intersection limit that marks components as belonging to the same channel. The easiest way to find the correct tolerance is to change ``--tolerance`` while having the ``--test`` option on to be able to quickly notice errors on a PNG and colored output. Once the right tolerance is found, the schematic can be generated normally as SVG.

When referring to schematic groups in the chip file, the layer index and group index within that layer should be provided:

.. code :: yaml

  # layer 1 and group 12
  schematic: [1, 12]

These must be determined by trial and error as they depend on the rendering order which can hardly be predicted.

Examples

.. code :: sh

  $ create-schematic --input 16-units-analog.dxf --maxsize 30000
  $ create-schematic --input biodisplay.dxf --maxsize 40000
  $ create-schematic --input transfection-device.dxf --minsize 0 --tolerance 0.05 # + group layers 1 and 2
