Protocol format
===============


Example
-------


.. code :: yaml

  ---
  name: MITOMI protocol 1

  # Valves aliases or groups (optional)
  aliases:
    reag1: C2
    reag2: C3, C4

  # Valves active by default (optional)
  defaults: A6, reag1

  # Function definitions (optional)
  functions:
    seq1:
      states:
        - duration: $3
          valves: $0
        - duration: $3
          valves: $1
        - duration: $3
          valves: $2

  # Protocol stages
  stages:
    - name: Initialization
      steps:
        - name: Initial water flow
          duration: 20 min
          valves: A1, A2, B1
        - name: Run sequence 1 on A1.A3
          call: seq1(A0, A1, [A2, A3], 2 sec)
        - valves: A1, [reag1]
          confirm: Please confirm something
          confirm: true # Or without description



Functions
---------

A function is a repeated set of actions with a fixed number of parameters which can be referenced by prefixing a ``$`` to the index of that parameter, starting at 0 with ``$0``. A parameter can only be of type ‘duration’ or ‘valve’, and this type will be determined implicitly when observing how this parameter is used.

Functions must be defined in the root property ``functions``.

.. code :: yaml

  functions:
    fn1:
      name: Function 1 # optional name
      actions:
        - valves: $0
          duration: $1
        - etc.

Calling a function can be achieved easily by replacing ``valves`` and ``duration`` properties in an action with a single ``call`` as follows:

.. code :: yaml

  call: fn1(A1, A2, 3 sec)

Functions can call other functions as long as they always reference a function defined earlier, excluding themselves. Recursive calls are therefore impossible as they would lead to an infinite loop in the absence of an if/else mechanism to break that loop.

.. tip ::

  To have a set of valves continuously activated during a function's call, you can define these valves as defaults and then restore the former defaults in the same action:

  .. code :: yaml

    call: fn1(...)
    defaults: + A4, A5
    restore:

  For details, see the :ref:`Defaults` section.


.. admonition :: Solving ambiguous arguments
  :class: caution

  If two arguments of type ‘valve’ are adjacent and you wish to pass more than one valve to one of them, the program won't be able to determine to which argument the middle valve belongs:

  .. code :: yaml

    call: func(A1, A2, A3)

  To solve this, you can isolate one of the arguments by enclosing it with square brackets:

  .. code :: yaml

    call: func([A1, A2], A3)
    call: func(A1, A2, [A3])
    # or
    call: func(A1, [A2, A3])
    call: func([A1], A2, A3)

  Furthermore, enclosing all the valves for one argument will result in the other being empty, which is also valid:

  .. code :: yaml

    call: func([A1, A2, A3])
    call: func([A1, A2, A3], )
    # or
    call: func(, [A1, A2, A3])

Implementation details
^^^^^^^^^^^^^^^^^^^^^^

Function names
  Function names must start with a letter (lower or uppercase) and can then contain an arbitrary number of letters, digits and underscores.

Calls
  The call syntax can contain any amount of whitespace as long as it doesn't affect individual values such as valve names.

Parameter references
  When referring to a parameter without having its previous counterpart present anywhere in the function (e.g. referring to ``$3`` in the absence of ``$2``), the missing parameter will be ignored (here the third argument would correspond to ``$3`` instead of ``$2``), however a warning will be emitted.



Defaults
--------


A set of default valves can be defined to avoid redundancy. Once a valve is set as a default, referencing that valve will open it instead of closing it.

The list of default valves acts like a stack. A ``defaults`` instruction will push a new set of defaults to the stack, while ``restore`` will pop the stack and restore defaults to their last value. When pushing to the stack with the ``+`` modifier, the new defaults will be XORed with the previous ones instead of simply replacing them. If both ``defaults`` and ``restore`` are referenced, the new defaults will act as a replacement of the current value in the stack if the action is a call.

Furthermore, if a function pushes sets of defaults, the stack will be reset to its position from before the call as soon as the function is done executing. The function also won't have access to the lower part of the stack from before it was called.

.. code :: yaml

  # new defaults: C2, C3
  - defaults: C2, C3

  # new defaults: C2, C4 (XOR applied thus C3 was removed)
  - defaults: + C3, C4

  # new defaults: C2, C3
  - restore:

  # activated valves: C3 (XOR applied on defaults and valves)
  - duration: 1 sec
    valves: C2

  # new defaults: none
  - restore:

  # raises an exception
  - restore:

Defaults can also be global to the protocol:

.. code :: yaml

  defaults: C1, C2

  stages:
    - defaults: C3   # new defaults: C3
    - defaults: + C3 # new defaults: C1, C2, C3


..
  Multiplexing
  ^^^^^^^^^^^^

  The combination of aliases and defaults is particularly useful when dealing with multiplexing, for example considering multiplexing valves ``mpx1`` to ``mpx8``:

  .. code :: yaml

    aliases:
      Multiplexer: mpx*
      Row1: mpx1, mpx3, mpx5, mpx7
      Row2: mpx1, mpx3, mpx5, mpx8
      Row3: mpx1, mpx3, mpx6, mpx7
      Row4: mpx1, mpx3, mpx6, mpx8
      # etc.
    stages:
      - defaults: Multiplexer # Close everything by default.
        steps:
          - valves: + Row1, Row3 # Open rows 1 and 3. Redundant valves mpx1 and mpx3 are not a problem.

  This code leverages De Morgan's law to enable addition of open valves.



Values
------


Durations
^^^^^^^^^

Durations can be specified in a human-readable syntax or as a number of seconds. All of these are equal::

  3 hours 2 minutes 5 seconds
  3 hrs 2 min 5sec
  3hr 2min5sec
  3h2m5seconds
  5s3h 2m
  3h 2.0833m
  3h 2m .0833m
  10925
  10925.0

.. note ::

  When specifying durations as a number of seconds such as ``10925``, the Yaml parser will interpret this value as a number, which might affect code highlighting. This is however not a problem when parsing the file, and numbers encoded as strings, e.g. ``"10925"``, are also allowed.


Valves
^^^^^^

Syntaxes include::

  A1 # valve A1
  A1.A3 # valves A1 to A3
  A* # valves that begin with A (and with at least two characters)

.. note ::

  The range syntax on valve names is based on `Python's algorithm <https://docs.python.org/3/tutorial/datastructures.html#comparing-sequences-and-other-types>`_, which in turn uses lexicographical ordering using their Unicode numeric value as returned by `the ord() function <https://docs.python.org/3/library/functions.html#ord>`_. This ordering should be fine as long as the compared characters belong to same set among A-Z, a-z and 0-9.


Implementation details
^^^^^^^^^^^^^^^^^^^^^^

Durations
  Supported time factors include ``s``, ``sec``, ``second`` and ``seconds`` for seconds, ``m``, ``min``, ``minute`` and ``minutes`` for minutes and ``h``, ``hr``, ``hrs``, ``hour`` and ``hours`` for hours. Scalar values can include a decimal part, have whitespace around, and can be ordered arbitrarily.



Python loader
-------------


For advanced customization, protocols can also be written in Python in addition to Yaml. The Python loader is however lower-level and has much less features (e.g. no aliases nor defaults) as part of them are directly built into the Yaml loader.

The protocol file should expose a single ``protocol()`` function, takeing a ``Chip`` instance as argument and returning a ``Protocol`` instance. This function should avoid side effects.


.. code :: python

  from library import *

  def protocol(chip):
    warnings = list()

    return Protocol(
      name="MITOMI protocol",
      stages=[
        Stage(name="Stage 1", steps=[
          Step(name="Step 1", action=State(duration=parse_duration("10sec"), valves=chip.resolve("C3.C9", warnings=warnings))),
          Step(name="Step 2", action=Actions(name="Block {func()}", actions=[
            State(confirm="Please confirm")
          ])),
          Step(name="Step 3", action=State(duration=parse_duration("10sec"), valves=chip.resolve("C1.C2, MITOMI", warnings=warnings))),
        ])
      ],
      warnings=warnings
    )

More advanced loaders can easily be built on the Python loader.
