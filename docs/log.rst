Log format
==========


.. code ::

  <time> [CHIP] <chip object as JSON>
  <time> [PROTOCOL] <protocol path as JSON>
  <time> [STATE] <state index> <progress> <valves state>
  <time> [PAUSE] <progress>
  <time> [RESUME]
  <time> [DONE]



Command flow
--------------

.. graphviz ::

   digraph foo {
      node [fontname="Helvetica", shape=box];

      chip -> protocol;
      protocol -> state;
      state -> state;
      state -> pause;
      pause -> resume;
      pause -> "state (paused)";
      "state (paused)" -> "state (paused)";
      "state (paused)" -> resume;
      resume -> state;
      state -> done;
      "state (paused)" -> done;
   }



Commands
--------


Pause
^^^^^

.. code ::

  <time> [PAUSE] <progress>

Creates the following event for the current state:

.. code ::

  {
    paused: true,
    progress: <progress>,
    time: <time>,
    valves: <same as previous>
  }


Resume
^^^^^^

.. code ::

  <time> [RESUME] <progress>

Creates the following event for the current state:

.. code ::

  {
    paused: false,
    progress: <same as previous>,
    time: <time>,
    valves: <same as previous>
  }



State
^^^^^

.. code ::

  <time> [STATE] <state index> <progress> <valves state>

Creates the following event for the current state:

.. code ::

  {
    paused: <same as previous>,
    progress: <progress>,
    time: <time>,
    valves: <valves state XOR state.valves>
  }
