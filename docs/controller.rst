Python API
==========


.. py:class :: Controller

  .. py:method :: chip_read_all(chip)

    :return: A 32-bit integer representing the state of valves defined by ``chip``.

  .. py:method :: chip_write_all(chip, state)

    :param state: A 32-bit integer representing the state of valves defined by ``chip``.

  .. py:method :: is_device_supported(product_id, vendor_id)
    :classmethod:

    :return: A boolean which is true when the controller is compatible with the device specified by ``product_id`` and ``vendor_id``.


.. py:class :: ControllerImplementation(ondisconnect = None)

  :param ondisconnect: An optional function that should be called when the connection is unexpectedly lost, for instance because the device stopped responding.

  Derives :class:`Controller`.

  .. py:method :: devices
    :property:

    List of ``{ 'product_id', 'vendor_id' }`` dictionaries that describe supported devices.

  .. py:method :: list()

    :return: List of ``{ 'id', 'name', 'description' }`` dictionaries that describe devices known to this controller.

  .. py:method :: connect(path)

    :param path: An identifier for the interface to which to connect. Usually a path on UNIX (e.g. ``/dev/cu.usbmodem1101``) and a string on Windows (e.g. ``COM1``).
    :raises: An exception if the device is inaccessible.

  .. py:method :: disconnect()

  .. py:method :: relay_read(self, channel)
  .. py:method :: relay_write(self, channel, value)

  .. py:method :: relay_read_all(self, state)
  .. py:method :: relay_write_all(self, state)
