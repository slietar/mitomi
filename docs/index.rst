Protocol runner
===============

.. toctree::
   :hidden:
   :maxdepth: 2
   :caption: Contents

   chips
   protocol
   controller
   jsapi
   log



Indices and tables
------------------


* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`



Supported devices
-----------------


Operating systems
^^^^^^^^^^^^^^^^^

* macOS with Safari >12 (minimal), >14 (best experience)
* Windows


Relay boards
^^^^^^^^^^^^

* Numato relay boards with at most 32 valves
* See the Controller class to add another controller
