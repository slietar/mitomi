# MITOMI automation


## Development

```sh
# Both
$ cd frontend/view
$ npm run build

# Browser
$ cd frontend/view/tmp
$ ln -s ../../../data data
$ open index.html

# Python app
$ cd backend/view
$ pip3 install -r requirements.txt
$ python3 main.py
# Press Ctrl+A/Q to exit and Ctrl+R to reload

# Building for macOS
$ cd backend/view
$ python3 setup.py py2app

# Building for Windows
$ cd backend/view
$ pyinstaller main.spec -y

# Building docs
$ cd docs
$ sphinx-build -M html . _build
$ find . -name '**.rst' | entr -r sphinx-build -M html . _build # to watch
```
